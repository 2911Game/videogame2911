# README

# Set up Eclipse with Git

* Download [eGit](http://www.eclipse.org/egit/) and work through [sections 1.1 - 1.3 of the tutorial](http://wiki.eclipse.org/EGit/User_Guide#Create_Repository_at_GitHub).
* Details of [adding your SSH key to your bitbucket account are here](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html).

# Remember to push and pull often! Especially remember to pull before you start working!

* pull before you push (I had this error)
* merge changes / ask people before you override code

# System

High-level overview of how this system works.

* `Game` is the main class. Sets up `JFrame` and menubar. Call other classes for `Board`, etc.
* `Board` is the class that draws a level onto the screen and has action listeners that makes the `Player` respond to the arrow keys.
    * should probably move levels out into their own class `Level`, esp. when going multi-level
* each level is a 2D array of `Item`s
    * the current window size **500 x 504 px** can fit `NUM_ROWS = 23` and `NUM_COLS = 25`
    * workflow to make a level
        * set `level = initBlankLevel()`, which creates an `Item[NUM_ROWS][NUM_COLS]` array with all `Item`'s = `Item.EMPTY`
        * use [google sheets](https://docs.google.com/spreadsheets/d/1gZdg7gdRpAAjXky03YvEllNqFsKZ_G1WTc8MggI-5Lo/edit?usp=sharing) to design levels
        * create a separate function to add all non-empty `Item`'s at the appropriate indices of the 2D array
        * `initWorld()` will create objects for those `Item`'s in the `level` 2D array and `drawWorld(Graphics g)` renders it as an image to the screen
* There are a bunch of items in the game: `Wall`, `Player`, `Box`, `Goal`, which are all subclasses of `Actor`
    * enum type `Item` lets you identify what an item is in a 2D array (i.e. when setting up levels)
    * reference the actual subclass of `Actor` when you want to refer to a specific object and it coordinates, etc. if it's colliding with anything
* Icons
    * should all be size **20 x 20 px**!!! Resize using online resizing tool before adding to `icons/` folder
    * can easily change icons for `Actor`s in their specific subclass: `URL loc = this.getClass().getResource("icons/brown_icon.png");`
    * need to make prettier icons, but the quality is really bad at 20 x 20 px so has to be pretty pixely
    * background color of the map is set in the first line of `drawWorld()`, `g.setColor(new Color(204, 230, 255));` (this is an RGB color code)
    * figure out how to make the `Box` sitting on top of `Goal` more of an overlay instead of just covering it completely

# To-Do

> Look at the Asana!

* Menu Bar
    * Reset not responding to clicking on the menu item, only the shortcut key. See [Swing Menus and Toolbars](http://zetcode.com/tutorials/javaswingtutorial/menusandtoolbars/)
    * Exit responds to clicks fine, though. Not sure what's going wrong there.
    * Need to implement an `undoMove` function
    * Need to implement some sort of `save` function. Not sure if we want to write to files or print something on screen.
    * Need to implement some sort of `loadLevel` function. Do this after we've set up multi-levels.
    * Need to have some sort of help dialogue pop up for Help
    * Need to have a shortcuts dialogue pop up for Shortcuts (although they're all appearing next to menu options at the moment)
* Icons
    * update to cuter icons at one point. I've added some icons to the icons folder, but only the colored tiles, red dot and red heart are sized to 20 x 20 px.
    * all icons must be **20 x 20 px**
* Score
    * need to implement a score and display it somewhere, preferably on the **menu bar** but not sure how to put a `JButton` with text on the menu bar that responds to an `ActionListener` for some sort of counter we need for `numMoves`
* Levels
    * make a pretty level for the Sprint review (modify `testLevel` to fill up the entire screen (some of the hardcode might break after the resize) and solvable)
    * figure out how to move from level to level
        * prob need a `Level` class and some kind of progression through an `ArrayList` of `Level`'s, `repainting()` a level every time a level `isDone == true` (pop up a dialogue showing completion to user, then click button that triggers this event of loading the next level)
        * if you move to a new class, move the constants `NUM_ROWS` and `NUM_COLS` to an `Enum` type (like `Collision` and `Item`)
    * figure out how to auto-generate levels

# Design Documents

* initial setup heavily based on [this Sokoban tutorial](http://zetcode.com/tutorials/javagamestutorial/sokoban/) (I think my comments are better than the ones here though)
* [Sprint Backlog and Test Level Planners](https://docs.google.com/spreadsheets/d/1gZdg7gdRpAAjXky03YvEllNqFsKZ_G1WTc8MggI-5Lo/edit?usp=sharing) (might want to move Test Level Planning sheets to a diff doc)
* [UML Class Diagram Link](https://www.draw.io/?lightbox=1&highlight=0000ff&edit=_blank&layers=1&nav=1&title=Ass%203%20Video%20Game%20UML%20Class%202.html#R7R1bc9o499cwwz7Q8ZXLIwSSZpc0mSbdtk8dgQV4aix%2FtiHJ%2FvpPsmVjSwY7xCC5oQ8NlmXpSOfo3CW19Kv1y40PvNUdsqDT0hTrpaWPW5qmml0D%2FyElr7TE0HpxydK3LVq2K3i0%2F4O0UKGlG9uCQa5iiJAT2l6%2BcI5cF87DXBnwffScr7ZATr5XDyxpj8qu4HEOnASOT%2Bau%2FLtthau4vK91d%2BWfob1cJX2r3UH8Zgbmv5c%2B2ri0x5amL6J%2F8es1SNqiPQcrYKHnTJE%2BaelXPkJh%2FGv9cgUdMr3JxCXfha8JtC19tArXDn5Q8c%2Fo9fWej9UqH%2BPR%2BdANs93ta69PodkCZ5M0qHUd%2FO3Iy%2FXT%2Fd%2BGjGi0Bv7Sdlv6EL9VvBf8f9SrEpd3QuTF74zMuxC%2BhB3g2Ev63RwDB%2F1dm%2FjXkv6Nep4lBTdgDZNCPIYZWxGXeWzZyidwJxSZdKHu7%2B2ocTpwEWYGyrbdIYSEgG%2FFdUbRzwNAnwqGJZ7A0Q6Om%2FRRACy2G4T%2BZh7ayA3iqrfZEtlRiv%2BCNSbukVP0pFwhOhhEgE1f5z8tJd7TAD6iqG9vkW39df7%2B6ar2qyzfU0KwyU%2FJGthuGzdAqfEx9G132TJHLXP8V1xE5isD7EaaAZD1FMDwmze2fUho7pXiVm6wD5LCnwEmI5230A9trJcMY%2Bk3juTjiMrCcQzYCOFaCydSIhY2Fs%2F6aIHckGpVqkafr8HadohC9hk6W0haLRP9VFkgMMCXTBFVBW4gWsPQf8VVkrfJJ1Tv63SpevC8U6E0k5atsupTjxYCqrgt07Z3ygf%2BQfWPPbqIyukiFiKdQ9Ib2MJIi4xULjxXmgJf5tAjwiPlr%2B4s8DKYsextgpgVcC2H6BwKqa58u5sSPcQBQUDUXBssfbBu6dcZnGY%2B5rBKFJr85PsQyycwiyoQ4vOQ7YbRXET8hOB8EyIqw1SeBIrpJPDAHPOkJ%2FIw7gwYwiD9EHq5Qg4WOgQwfdGfw%2FmcfBn66DfMvJn1TcNUdm1Oo37H2q5khMIQ4UkYG7uyrxTFWj301dESgkoITBv0OQpTuwUUptVBYJrMyu4dZgNDC3hx7ebpvG%2BT85KATOr%2Bhq8PeP0G0GrDuPI%2F8HWyxXgrlqd%2FDLc39dxiNPo8t1f7Jr8W%2B3WsRb1gLTLTCV1rSJwB%2BAl50I3YGvDDpGxu%2B3MHPjgbDMYI172OZjNiv1G9zDN%2BSye4n59Glp%2FundYAbfw5zPMR3MkSMnMCrcQzsWfyixidDx0Q2luY67FodmlzD0S67BCZul0oInWVQVAMPP0q6w1gGtJ7JQ3FI%2BYaipCdjrEa%2Fg0O%2F3jSyaSuyP8z9AKJaF5E5tTdv1eEkyLfj5xF1xUEvk2%2BtsPMZ7HUJx8rU%2Fy88aMOVsCiFSWW%2FEYtkr8GljFgxLeuFbCMIvGt18EyTJnFN5YZF%2BktJcjNFtIaw5KVghWnFKy4WoR0l1txk5cQC9PgkKyeOWj%2B%2B2lFEMsI3ryQPiCG4Ysd%2FiDVPmkmffxJvzpeQpviJLTK2D1d5VgJrZU0VKOE7nPIv8Jo8pETGdIM%2FoMV8MjPzdq59onbXh89r%2BwQPmJBRsqffeAdqXq9wb7UmXnWChwYvSIHhm7UsVwGMguoIud%2Fc%2BTTWyMqUgDdan7MIJroNjvzHzKIQOpaPnj%2BjnzHai9pSI1E0O15UOoqODVkHsAsXyqo5MDYfBOEaD3a2I4VHAiSnBEiH66Jq89xRpswxCygGlSN1mI7up4XzB2TF8ypGyPv961BMCdmq5yC%2Be9YYWqiVL5YjXKuN03p5pebppnceusXLLeeWcNq4%2BN4Va3GVg0WI2sw4ln0XzOvyOPPsqnOGZMKb0wmHEWIu5dhpeqxxqTaL2upPmtyIHXs7e8H4JIcyAsLlgnkRrPgjmoyqytduCU8uA7P3YAPrzWbBw8KeLAmjgd32EjI0Uy4d0YezMfc%2BHxiPuiawfiOMKKllAvHWjZYI9eijt%2FoVVLXaOVDshXCYDOajbKfyLIk9qlnZqlMzdJY8hJTySMdJvLDFVoiFziTXenxykBCnKWEmBCP8mmgJshI8N6vhy6ZULB5rGpgHm5nD1FiWgCvmWo0GLoXXGYVdfsHgWJqJytltyDi7pmPE1jQYhHAdy8hPiIiVI055F4cush9XaMNCXOf1blYr242jBLEp3aAxdcluHo%2BkEldEM39A1apkL%2Fe5UfFKPkAKVKc3yqNE2X9VnqBEseJ8aO0uF4BtymV13IkSSWc8ihBeQqNTWNQmcYG3yobO72ShmrU2PgY7L82fOaIQM7oa0crWC2F4VeD1S%2BOWi18%2BJVs8SAsL%2BpJU1wIrRZNIp%2BRNLPoLY0lMlMqb6bXYqEVZ3pZ3VnX7NaDTL3HbAVI8wGya7NwL0ANqEy5pyx6Vl4laXYY%2FRk4Dt3zFCnMRLXKK4%2FfAcGGyAB0HO4nOaCH4ByhF%2FFghtCBHrYoD4P6RGuJh3eJQAn6b3AN8XCSIOlhOOmOPfGQUrX4wQGvOQvlbBCgcAV92r1AUN5m55xqMoI58ilGIg3s%2FBDYwRi5FIQZQg4Erggw5hufaADkfIe4ZvRLACAO3ELnBtI2kifogyjzSRA8cZ3bEK6p%2Fhb%2FL4JcXDu0gTM9Diop9IpWszPd4iw3IdvjEwKIs8qEJyiJZuAxPlYguF1jBYBYW1jJa4OIchLfEyEisiHIija%2Fkw3A0Ztx8viXUM4bj2DjWSA8HnZx2Wl46hNdtYHTvoTh%2B6DP6OliJp%2FYXtimd%2BwAA9VADOAhxMpnUwchBwe0gyu09hwYQgmEAqnrw8hfHCkpckC0cS30GEJPPDRykEwAtjBzzI9g7DgIWDJA04lo18Msaeg45IwFCbLARdNLZBySBAkoyXIWPSGp8nCXo1dRXhQKzP3OqyMLVMCyyBpqu%2FCZ%2FI3rx3444RzHduc%2BXGOyfiQOoAtRJ5blyAHub26hy%2BL4eMJ6xYUL5Rc%2BllQEPTlJtccHThVq8S4UDDa37gS4X3ecHFk5WET4PqleRoFZ7wARzysjD3V2foQajc8rEAa37o1vW%2B2XDO0QO%2FE1fc5yLlGYLAExfrbD9Q7UPR7FRudF9djcdr3g7CijKDugl5wt%2Fb70AKmPgtlzru2M%2FUBmb%2F4bRKEsIDc5AJElmUNxiEZzjQ6bHzbgM4qKcimTjR7vYxl85nYDNz%2Fs2eFgHkJQ2faFlJu%2BYf%2BCOdD1Qly%2BN0uTESwqey5L5Q0MaklDe5I0T7RvQFX4VN7Tb8iqRBIp%2BqXZW2UwbILdEFeZBtidsmxD9SXqpuTWbPZC91aprezeKi23t0r5pChGnvsYelrjAfo2njaShvSG07oKmZLyRqaUz1fVayFFg9Vzj2VHXbWkoTpJUZNZOU4cFs3TijsxOgU5NjqMnXn%2B3j17Tq3aNbl%2BR3Z0NdwiiNbJQWfIB%2FWPYmUsIsC2vc6Qo3CP2jIBK%2BtjL1sofziefmToVziCMDw%2F25wzUSjB%2FJDFWf5TOCBy0OwKBLu0pSipngZf6klbarT3RjWZ44J6Bue%2BSY%2BnzR3hVsflMKrKn2gixsBSeBxVNrAqGuRF9lCi1Qu1h1T%2ByCj2ZPfKxrnONXVKm4g%2F%2BkQim4huuGqeSfQ2vi0LyE02CwipvN0qaLbgMZiDX0yDlzxq0eGhg1oEj9SxxngL7IVzXDhHGeeIKEW5eBT4mSE7jcumpsRYbDaDZW%2BVMPUCBlt41oNWC4eV7FCtPIfl0m0vTPbCZPexEpqtXZYg5kXVbsc0XeyD8962tJshk7M92hYMwgdAr8BI9gj%2B4VKhyxyYZWo9XioMiqRCkhvyPqlQdPiZNFKhYJ9oc%2BQCieYRis6Ts%2FRgN1w2pBuTLyo4NzcBDMeYINsrmBzdUpnJniE8EoGWiZAcXjHN5vp9owLX7xVy%2FVpsgb7MXJ8ejNY8jn%2BxBM7NNgipfDBHLZ4PoY5a%2FvxPkXce7J3QXKxPLUi%2BrR7rO8VVpawzSGd37FSO8Bn9kpZqDPAlQDcL%2BYZsyDeZFWzoxyK%2Fp5S0VCfy5bpxqiLyCw7IFov8HrvyB8civ8%2BufLalOpGvNRH5PdmQ32eQb6jHIn%2FQL2mpTuTLdc9RReT3ZUP%2BgGXWypHI15RuSUt1Il%2BypJ5D5srE3azJgafEyX1mc6VeEzh1zjfSDv72cG5rcHz%2F%2Fcu5%2B5xOrp%2FO3efX25vPZ%2B%2B0qluj0WZ1l7lvxEhYbFYSFCXeqrVcxaBJlv%2F0Ibgsc8ZJcxjsw3T4c%2FL13Hzg%2B3A6PXefo%2Fsf5%2B7y5n549mFO7h6efp6706fJdPJw%2F3WPPGk0N%2BfsLLMqN2fT44%2Fj5pLlWn0Ibs6cPdYcbj4ZPp597d9Nxrff7s7d6%2Bfh1%2FHZR%2Fpt%2BnQrRmA%2B3n65mU5%2Bien86%2FDL%2BP7ulwjiol2LoTHauQhSu%2Fr2%2BETGfT%2Be%2FHqaPJ7dUMv2fzf8B1PfHyjaB6zvq8hQ6xaIdq2Oy0JVjU%2BYa94OyX03xVf3rhbunkxchUJ3T7KHwBhs9nzlO%2BNZjwDbUJ1OVj6qfqEqxjUilKq4cMuxVKWqbPD3hGSVJItdyKqArLoSkJWqskyGFVLVd3rrJS3VSVd8KgCxvhyOtqS4MtlkZqZjFNyYrHaLdAa1Hu%2BuzkfPXbyEyDHVWzi0gBdZuORgjNTidmeBl1EXLXub6G8%2BXEA8G3MYtOgty9zmqUxtDiXyXrncn8PiK5dnfZM4cGohBnWQP1Kza2o8LRR5hmpJvNX5SPrFMXRqx9B0i8msob4hEWbkaDr88s%2B5OyUD%2FRX5as7d88X9xsmIZnkEVJU5QLR67LYWb7%2FOZ8hctGxG4AnWslkCOV7LZr0Lp9SyJcsJYKTqvgummyNc33FLtRTwlyhwsm%2FeyVOQmIuh092H8e1Y8%2FQW91RpJDuKgs0sQyfMJTkCr%2FdKF6VfZc2dagnJe%2F2ZPPMjzQVsWYDuNk5ovwGqZmuJ7EkLhdcpaYVHLdSyc06X5W6UetTEnJJY%2FW6UPWqiDD5%2BVWOOwOtwTp%2Fqu%2FLYOw24po5WFPGjj8ia3lX3gbeKHbD65P8%3D)

![UML Class Diagram](https://dl.dropboxusercontent.com/u/72546174/COMP2911/Ass3/Ass%203%20Video%20Game%20UML%20Class%203.jpg)

# Help! What is Swing?

* [Zetcode Swing Tutorial](http://zetcode.com/tutorials/javaswingtutorial/) is a good general guide
    * [Zetcode Java 2D Games Tutorial](http://zetcode.com/tutorials/javagamestutorial/)
    * [Java 2D Tutorial](http://zetcode.com/gfx/java2d/)
* [Oracle Java Guides for Swing](https://docs.oracle.com/javase/tutorial/uiswing/TOC.html) to look up more info for something specific
* [Java SE 8 docs](https://docs.oracle.com/javase/8/docs/api/) for specific method listings