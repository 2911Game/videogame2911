import java.awt.Image;

/**
 * Any "Actor" in the game. Superclass for Wall, Player, Box, Goal, Lava.
 * Codes the basic functionality for any object in the game.
 * 
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public class Actor {
	
	private int x;		// x-coordinate of bottom-right corner of square
	private int y;		// y-coordinate of bottom-right corner of square
	private Image pic;	// image representing this Actor
	
	/**
	 * Simple constructor for an Actor. 
	 * Specify the coordinates (in terms of pixels) of bottom-right corner of square for this icon.
	 * 
	 * @param x		x-coordinate of bottom-right corner of square
	 * @param y		y-coordinate of bottom-right corner of square
	 */
	public Actor (int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Setter for the Image representing this object
	 * 
	 * @param im	Image to represent this object
	 */
	public void setImage(Image im) {
		pic = im.getScaledInstance(Dimensions.SPACE, Dimensions.SPACE, Image.SCALE_DEFAULT);
	}
	
	/**
	 * Getter for the Image representing this object.
	 * 
	 * @return	Image that represents this object
	 */
	public Image getImage() {
		return pic;
	}
	
	/**
	 * Setter for the x.
	 * 
	 * @param x	value you want to set to
	 */
	public void setX (int x) {
		this.x = x;
	}
	
	/**
	 * Setter for the y.
	 * 
	 * @param y value you want to set to
	 */
	public void setY (int y) {
		this.y = y;
	}
	
	/**
	 * Getter for the x.
	 * 
	 * @return x value
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Getter for the y.
	 * 
	 * @return y value
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Checks if this Actor collides with another Actor in a given direction.
	 * 
	 * @param other	    potential other Actor it might collide with
	 * @param direction which direction from this Actor are we looking at
	 * @return true if other and this Actor are touching on this Actor's given direction / side;
	 *           false otherwise
	 */
	public boolean hasCollision(Actor other, Direction direction) {
		if (((x - Dimensions.SPACE) == other.getX()) &&
			(y == other.getY()) && direction.equals(Direction.LEFT)) {
			return true;
		} else if (((x + Dimensions.SPACE) == other.getX()) &&
		           (y == other.getY()) && direction.equals(Direction.RIGHT)){
			return true;
		} else if (((y - Dimensions.SPACE) == other.getY()) &&
				   (x == other.getX()) && direction.equals(Direction.UP)) {
			return true;
		} else if (((y + Dimensions.SPACE) == other.getY()) &&
				   (x == other.getX()) && direction.equals(Direction.DOWN)) {
			return true;
		}
		return false;
	}
	
}
