import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JFileChooser;

/**
 * This is the "Model" for the Game (in terms of MVC architecture).
 * Contains a matrix representation of Items for the map of the game.
 * Checks collisions, has scores, generators, saves game data, and loads game data.
 * 
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public class Board {
	
	private ArrayList<Wall> walls;	// change to private
	private ArrayList<Box> boxes;
	private ArrayList<Goal> goals;
	private ArrayList<Lava> lavas;
	private ArrayList<String> moves;
	private ArrayList <String> movesMultiplayer;
	private Player me;
	private Player otherPlayer; //This is for multiplayer
	
	private int score;			// number of moves taken / score
	private boolean isDone;		// are you done with this level?
	private Mode currentMode;
	
	private LevelGenerator generator;
	protected Item[][] level;		// each level is represented by a 2D array of fixed size (NUM_ROWS x NUM_COLS)
	private Item[][] initialLevel; // this is the starting position of a level (to replay moves in save/load)
	private LvlCode currLvlType;	// current level type to generate
	
	//For Single Player
	private String[] gameLevels; //store all the game levels available to play
	private int levelNum; //level number. increment when a level is completed
	private boolean createLevelInit;
	private boolean createLevelInitLoad;
	
	//For Multi Player
	private String [] multigameLevels;
	
	// constructor
	public Board() {
		// initialize empty array lists for items that populate the map
		walls = new ArrayList<Wall>();
		boxes = new ArrayList<Box>();
		goals = new ArrayList<Goal>();
		lavas = new ArrayList<Lava>();
		moves = new ArrayList<String>();
		movesMultiplayer = new ArrayList <String>();
		
		// set the score up to 0
		score = 0;
		// have we completed this level yet?
		isDone = false;
		
		//set to first level
		levelNum = 0;
		
		//get string of all game level text files for single Player
		File singlePlayerDir = new File("./singlePlayerLevels/");        
		gameLevels = getAllFiles(singlePlayerDir);
		
		File multiPlayerDir = new File("./multiPlayerLevels/");
		multigameLevels = getAllFiles(multiPlayerDir);
		
		generator = new LevelGenerator();
		level = generator.getLevel(LvlCode.BLANK);	// intialize these together
		initialLevel = generator.getLevel(LvlCode.BLANK);
		currLvlType = LvlCode.EASY;	// default is easy
		
		readLevel("StartScreen"); //initialize board using data in the text files
		
		this.createLevelInit = false;
		this.createLevelInitLoad = false;
				
		initWorld();
		
	}

	
	/**
	 * Converts the 2D array representation of the level into data items to be rendered.
	 * 
	 * @post Actor items will be initializes appropriately and added to its respective ArrayList
	 */
	public void initWorld() {
		int i, j;	// rows, columns (counters)
		int x, y;	// coordinates (remember hangs to the bottom right corner)
		int noPlayersDetected = 0; //This is to determine how many players are initially on the map
		Item it;	// Item at x and y
		isDone = false;
		
		// temporary objects
		Wall wall;
		Box box;
		Goal goal;
		Lava lava;
		
		// iterate through level and create objects as necessary, store in fields
		y = 0;
		for (i = 0; i < Dimensions.NUM_ROWS; i++) {
			x = 0;
			for (j = 0; j < Dimensions.NUM_COLS; j++) {
				it = level[i][j];
				
				if (it.equals(Item.WALL)) {
					// make a wall
					wall = new Wall(x, y);
					walls.add(wall);
				} else if (it.equals(Item.BOX)) {
					// make a box
					box = new Box(x, y);
					boxes.add(box);
				} else if (it.equals(Item.GOAL)) {
					// make a goal
					goal = new Goal(x, y);
					goals.add(goal);
				} else if (it.equals(Item.PLAYER)) {
					// make a player
					if(noPlayersDetected == 0)
						me = new Player(x, y, noPlayersDetected);
					else
						otherPlayer = new Player (x, y, noPlayersDetected);
					
					noPlayersDetected++;
				} else if(it.equals(Item.LAVA)) {
					lava = new Lava(x, y);
					lavas.add(lava);
					
				}
				// else, don't do anything if empty
				x += Dimensions.SPACE;
			}
			
			// start a new line
			y += Dimensions.SPACE;
		}
	}
	
	// ============================= Getters and Setters =================================
	/**
	 * Sets the level code for the current level.
	 * 
	 * @param type of level (LvlCode)
	 */
	public void setLevelType(LvlCode type) {
		currLvlType = type;
	}
	
	/**
	 * Get the current level number.
	 * 
	 * @return the current level number
	 */
	public int getLevelNum() {
		return levelNum;
	}

	/**
	 * Sets the level number.
	 * 
	 * @param levelNum integer level number to set to
	 */
	public void setLevelNum(int levelNum) {
		this.levelNum = levelNum;
	}
	
	/**
	 * Returns the coordinates of all Boxes in the game in terms of (col, row) entries of the level matrix.
	 * 
	 * @return ArrayList<Coordinate> of the boxes in the game.
	 */
	public ArrayList<Coordinate> getBoxCoordinates() {
		ArrayList<Coordinate> boxCoords = new ArrayList<Coordinate>();
		
		for (Box b: boxes) {
			boxCoords.add(new Coordinate(b.getX() / Dimensions.SPACE, b.getY() / Dimensions.SPACE));
		}
		
		return boxCoords;
	}
	
	// some getters and setters for the old updateState to be moved to Controller in Game
	/**
	 * Returns the player number for the first player.
	 * First player is the one controlled by arrow keys.
	 * 
	 * @return 0
	 */
	public int getMe() {
		return 0;
	}
	
	/**
	 * Returns the player number for the second player.
	 * The second player is the one controlled by WASD keys.
	 * 
	 * @return 1
	 */
	public int getOtherPlayer() {
		return 1;
	}
	
	/**
	 * Gets the x-coordinate in terms of pixel number of the player of playerNum.
	 * 
	 * @param playerNum 0 for first player, 1 for WASD controlled second player
	 * @return x-coordinate in terms of pixels of this player
	 */
	public int getPlayerXPos(int playerNum) {
		Player p;
		if (playerNum == 0) {
			p = me;
		} else {
			p = otherPlayer;
		}
		
		return p.getX();
	}
	
	/**
	 * Gets the y-coordinate in terms of pixel number of the player of playerNum.
	 * 
	 * @param playerNum 0 for the first player, 1 for WASD controlled second player
	 * @return y-coordinate in terms of pixel of this player
	 */
	public int getPlayerYPos(int playerNum) {
		Player p;
		if (playerNum == 0) {
			p = me;
		} else {
			p = otherPlayer;
		}
		
		return p.getY();
	}
	
	/**
	 * Add a move to the past moves string of the first player.
	 * 
	 * @param newMove String representing what this new move was.
	 */
	public void addMove(String newMove) {
		moves.add(newMove);
	}
	
	/**
	 * Add a move to the past moves string of the second player.
	 * 
	 * @param newMove String representing what this new move was.
	 */
	public void addMoveMultiplayer (String newMove){
		movesMultiplayer.add(newMove);
	}
	
	/**
	 * Increments the score
	 * 
	 * @post score is 1 greater than it was previously.
	 */
	public void incrementScore() {
		score++;
	}
	
	/**
	 * Get all the items in this board.
	 * 
	 * @return ArrayList of all the Actors on this board.
	 */
	public ArrayList<Actor> getAllItems() {
		ArrayList<Actor> allItems = new ArrayList<Actor>();
		allItems.addAll(walls);
		allItems.addAll(goals);
		allItems.addAll(boxes);
		allItems.addAll(lavas);
		allItems.add(me);
		if(currentMode == Mode.MULTIPLAYER || currentMode == Mode.GENERATION_MULTI)allItems.add(otherPlayer);
		
		return allItems;
	}
	
	/**
	 * Get the current score.
	 * 
	 * @return the current score
	 */
	public int getScore() {
		return score;
	}
	
	/**
	 * Get the current mode.
	 * 
	 * @return the current mode
	 */
	public Mode getMode(){
		return this.currentMode;
	}
	
	/**
	 * Set the mode.
	 * 
	 * @param m the mode to set the current mode to
	 */
	public void setMode(Mode m){
		this.currentMode = m;
	}
	
	/**
	 * Checks if this level has been completed.
	 * Gets the value of the field isDone set by isCompleted.
	 * 
	 * @see isCompleted
	 * @return value of isDone field
	 */
	public boolean isDone() {
		return isDone;
	}
	
	/**
	 * Returns the Item code of what is in the x,y coordinates of the grid.
	 * 
	 * @param x	x-coordinate 
	 * @param y y-coordinate 
	 * @return the Item code of what's inside this coordinate pair
	 */
	public Item whatsInGrid(int x ,int y){
		return level[x][y];
	}
	
	/**
	 * Set the Item code of the specified entry in the level matrix.
	 * 
	 * @param x	x-coordinate
	 * @param y y-coordinate
	 * @param itm Item code
	 */
	public void setGrid(int x , int y, Item itm){
		level[x][y] = itm;
	}
	
	
	// ===================== Collision Detection and Movement =================================
	
	/**
	 * Wrapper for hasImmovableBox that specifies player number instead of Player object.
	 * 
	 * @param playerNum	0 for the one controlled by arrow keys, 1 for the one controlled by WASD
	 * @param direction direction to move the player of playerNum in
	 * @return true if the movement results in an immovable box collision, else false
	 */
	public boolean hasImmovableBoxWrap(int playerNum, Direction direction) {
		Player p;
		if (playerNum == 0) {
			p = me;
		} else {
			p = otherPlayer;
		}
		
		return hasImmovableBox(p, direction);
	}
	
	/**
	 * Checks if the Player moving runs into an immovable Box.
	 * A Box can only move if it collides with a Player and it is not also
	 *   collided with another Box or Wall.
	 * 
	 * @post a Box that can move (returns false) will be moved by this function
	 * @post level completion is checked if a Box is moved
	 * @param direction	direction of Collision overall, relative to the whole game
	 * @return true if the Box cannot move, else false
	 */
	private boolean hasImmovableBox(Actor actor, Direction direction) {
		Box thisBox, other;
		
		for (int i = 0; i < boxes.size(); i++) {
			thisBox = boxes.get(i);
			
			if (actor.hasCollision(thisBox, direction)) {
				// box is touching Player, check if the box can move
				
				// check if this box is collided with another box in the same direction
				for (int j = 0; j < boxes.size(); j++) {
					other = boxes.get(j);
					
					if (!thisBox.equals(other)) {
						
						// only check collisions with boxes other than itself
						if (thisBox.hasCollision(other, direction) == true) {
							// are collided, so cannot move
							return true;
						}
					}
				}
				
				// check if this box is collided with a wall in the same direction
				if (hasWallCollision(thisBox, direction)) {
					// is collided, so box cannot move
					return true;
				}
				
				// check if this box is collided with an edge in the same direction
				if (hasEdgeCollision(thisBox, direction)) {
					// is collided, so box cannot move
					return true;
				}
				
				if (hasPlayerCollision(thisBox, direction)) {
					// is collided, so box cannot move
					return true;
				}
				
			}	
		}
		
		return false;	// box can move
	}
	
	/**
	 * A wrapper for moving a Player for outside classes.
	 * 
	 * @param 	playerNum	number of the player we want to move: 
	 * 			0 for the one controlled by arrow keys, 1 for the one controlled by WASD
	 * @param direction	direction to move the player in
	 */
	public void movePlayer(int playerNum, Direction direction) {
		Player p;
		if (playerNum == 0) {
			p = me;
		} else {
			p = otherPlayer;
		}
		
		p.move(direction);
	}
	
	/**
	 * A wrapper for updateBox for outside classes to call if a player collision causes a box move.
	 * 
	 * @param playerNum	number of the player that caused the move, either 0 or 1.
	 * @param direction	direction the Player of playerNum moved in. 
	 */
	public void moveBox(int playerNum, Direction direction) {
		Player p;
		if (playerNum == 0) {
			// single player
			p = me;
		} else {
			// other player
			p = otherPlayer;
		}
		
		updateBox(p, direction);
	}
	
	/**
	 * Updates the Boxes a Player runs into when moving.
	 * @pre the previous check of whether or not there is an immovable box has
	 *   been completed.
	 * @post any Boxes that can be moved will be moved by this function
	 * @post level completion is checked if a Box is moved
	 * @param direction		direction of Collision overall, relative to the whole game
	 */
	private void updateBox(Actor actor, Direction direction) {
		Box thisBox;
		
		// iterate through all the boxes and see if they need to be moved (and move if needed)
		for (int i = 0; i < boxes.size(); i++) {
			thisBox = boxes.get(i);	
			//System.out.println("Testing box " + i);
			
			if (actor.hasCollision(thisBox, direction)) {
				//System.out.println("=== Box has a collision! Moving... ===");
				// box is touching Player, so this one needs to move
				if (direction.equals(Direction.LEFT)) {
					thisBox.move(-Dimensions.SPACE, 0);
				} else if (direction.equals(Direction.RIGHT)) {
					thisBox.move(Dimensions.SPACE, 0);
				} else if (direction.equals(Direction.UP)) {
					thisBox.move(0, -Dimensions.SPACE);
				} else if (direction.equals(Direction.DOWN)) {
					thisBox.move(0, Dimensions.SPACE);
				}
			}	
		}
	}
	
	/**
	 * Wrapper for hasWallCollision where the user specifies player number.
	 * Don't allow classes outside the model to access Player objects.
	 * 
	 * @param playerNum	number of the player, 0 for one controlled by arrow keys,
	 * 					1 for the one controlled by WASD keys
	 * @param direction	direction to move the player in
	 * @return	true if there is a wall collision, else false
	 */
	public boolean hasWallCollisionWrap(int playerNum, Direction direction) {
		Player p;
		if (playerNum == 0) {
			p = me;
		} else {
			p = otherPlayer;
		}
		
		return hasWallCollision(p, direction);
	}
	
	/**
	 * Checks if the given actor collided with a wall.
	 * 
	 * @param actor	the Actor item that was moved
	 * @param direction	the direction relative to the Actor that the collision may occur
	 * @return	true if the Actor did collide in the given direction, else false
	 */
	private boolean hasWallCollision(Actor actor, Direction direction) {
		
		Wall wall;
		boolean hasCollision = false;

		// go through all the walls and see if the actor is touching the wall relative to the given direction
		
		for (int i = 0; i < walls.size(); i++) {
			wall = walls.get(i);
			
			if (actor.hasCollision(wall, direction)) {
				// the actor is touching this wall in the given direction, so moving would collide
				hasCollision = true;
			}
		}
		
		return hasCollision;
	}
	
	/**
	 * Wrapper for hasEdgeCollision using player number instead of Player object.
	 * 
	 * @param playerNum 0 for first player, 1 for second player  (WASD keys)
	 * @param direction to move player in
	 * @return true if this results in an edge collision, false otherwise
	 */
	public boolean hasEdgeCollisionWrap(int playerNum, Direction direction) {
		Player p;
		if (playerNum == 0) {
			p = me;
		} else {
			p = otherPlayer;
		}
		
		return hasEdgeCollision(p, direction);
	}
	
	/**
	 * Checks if the given actor collided with an edge of the screen.
	 * 
	 * @param actor	the Actor item that was moved
	 * @param direction	the direction relative to the Actor that the collision may occur
	 * @return	true if the Actor did collide in the given direction, else false
	 */
	private boolean hasEdgeCollision(Actor actor, Direction direction) {
		int newX = actor.getX();
        int newY = actor.getY();
    	
    	if (direction.equals(Direction.UP)) {
            newY -= Dimensions.SPACE;
        } else if (direction.equals(Direction.DOWN)) {
            newY += Dimensions.SPACE;
        } else if (direction.equals(Direction.LEFT)) {
    		newX -= Dimensions.SPACE;
        } else if (direction.equals(Direction.RIGHT)) {
        	newX += Dimensions.SPACE;
        }
    	
    	if (!(newX < 0 || newX > Dimensions.SPACE * (Dimensions.NUM_COLS - 1)||
        	 newY < 0 || newY > Dimensions.SPACE * (Dimensions.NUM_ROWS - 1))) {
        		return false;
        }
    	
    	return true;
	}
	
	/**
	 * Wrapper for hasPlayerCollision using player number instead of Player object.
	 * 
	 * @param playerNum	0 for first player, 1 for second player (WASD keys)
	 * @param direction to move the player in
	 * @return true if this results in a collision with another player, false otherwise
	 */
	public boolean hasPlayerCollisionWrap (int playerNum, Direction direction) {
		Player p;
		if (playerNum == 0) {
			p = me;
		} else {
			p = otherPlayer;
		}
		
		return hasPlayerCollision(p, direction);
	}
	
	
	/**
	 * Only Applies to Multiplayer. Checking whether players collide together
	 * 
	 * @param actor	current Player to test if collision has occurred
	 * @param direction	direction to check whether collision has occurred
	 */
	private boolean hasPlayerCollision (Actor actor, Direction direction){
		if (!(currentMode.equals(Mode.MULTIPLAYER) || currentMode.equals(Mode.GENERATION_MULTI))) {
			return false;	// can't collide with yourself
		}
		
		if (actor.hasCollision(otherPlayer, direction)){
			return true;
		}
		
		if (actor.hasCollision(me, direction)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Tests if this box is touching a Lava object.
	 * 
	 * @return true if the box is on a Lava object, false otherwise
	 */
	public boolean boxHasLava() {
		Box thisBox;
	//	Player thisPlayer;
		Lava thisLava;
		
		int numBox = boxes.size();
		int numLava = lavas.size();
		
		//check if box is on top of lava
		for (int i = 0; i < numBox; i++) {
			thisBox = boxes.get(i);
		
			for (int j = 0; j < numLava; j++) {
				thisLava = lavas.get(j);
				if ((thisBox.getX() == thisLava.getX()) 
					&& (thisBox.getY() == thisLava.getY())) {
					// are on top of each other
					return true;
				}
			} 
		}
		return false;
	}


	/**
	 * Wrapper for playerHasLava that uses playerNum instead of a reference to Player object.
	 * 
	 * @param playerNum	0 for first player, 1 for WASD controlled second player
	 * @param direction	to move the player in
	 * @return true if the move results in the player colliding with lava, false otherwise
	 */
	public boolean playerHasLavaWrapper(int playerNum, Direction direction) {
		Player p;
		if (playerNum == 0) {
			p = me;
		} else {
			p = otherPlayer;
		}
		
		return playerHasLava(p, direction);
	}
	
	/**
	 * Tests if the actor is colliding with a Lava object.
	 * 
	 * @param actor		the reference to the actor
	 * @param direction	direction the actor wants to move to
	 * @return 	true if the movement in the direction would cause the player to collide with lava,
	 * 			false otherwise
	 */
	private boolean playerHasLava(Actor actor, Direction direction) {
		Lava thisLava;
		
		int numLava = lavas.size();
		
		for(int i = 0; i < numLava; i++) {
			thisLava = lavas.get(i);
			if(actor.hasCollision(thisLava, direction)) {
				return true;
			}
		}

		return false;
	}

	
	// ========================== Transitioning between levels ==================================
	/**
	 * Checks if this level is completed.
	 * 
	 * @post if the level is completed, field isDone = true, otherwise nothing changes
	 */
	public void isCompleted() {
		int num = boxes.size();		// number of boxes we have to move to Goal squares
		int completed = 0;			// number of boxes we have successfully moved to Goal squares
		
		Box thisBox;
		Goal thisGoal;
		
		if (currentMode == Mode.START_SCREEN) {
			for (int i = 0; i < num; i++) {
				thisBox = boxes.get(i);
				for (int j = 0; j < num; j++) {
					thisGoal = goals.get(j);
					if ((thisBox.getX() == thisGoal.getX()) 
						&& (thisBox.getY() == thisGoal.getY())) {
						// are on top of each other
						int chosenX = thisBox.getX();
						int chosenY = thisBox.getY();
						System.out.println(chosenX);
						System.out.println(chosenY);
						if (chosenX == 240 && chosenY == 320) {
							System.out.println("User is in Single Player mode");
							currentMode = Mode.SINGLE_PLAYER;
							levelNum = 0;
							goals.clear();
							boxes.clear();
							walls.clear();
							lavas.clear();
							moves.clear();
							score = 0;
							
							readLevel(gameLevels[levelNum]);
							initWorld();
							isDone = false;
							levelNum++;
							return;
						} else if (chosenX == 560 && chosenY == 320) {
							System.out.println("User is in Multiplayer mode");
							currentMode = Mode.MULTIPLAYER;
							levelNum = 0;
							
							goals.clear();
							boxes.clear();
							walls.clear();
							lavas.clear();
							moves.clear();
							movesMultiplayer.clear();
							score = 0;
							readLevel(multigameLevels[levelNum]);
							initWorld();
							isDone = false;
							levelNum++;
							return;
							
							
						} else if (chosenX == 240 && chosenY == 520) {
							System.out.println("User is in Create Level mode");
							currentMode = Mode.CUSTOM_MODE_MAKING;
							this.createLevelInit = true;
							goals.clear();
							boxes.clear();
							walls.clear();
							lavas.clear();
							moves.clear();
							score = 0;
							isDone = false;
							initWorld();
							
						} else if (chosenX == 560 && chosenY == 520) {
							System.out.println("User is in Load Level mode");
							currentMode = Mode.LOAD_SCREEN;
							goals.clear();
							boxes.clear();
							walls.clear();
							lavas.clear();
							moves.clear();
							score = 0;
							isDone = false;
							initWorld();
						}
					}
				}
			}
		}
		
		if (currentMode == Mode.LOAD_SCREEN) {
			for (int i = 0; i < num; i++) {
				thisBox = boxes.get(i);
				for (int j = 0; j < num; j++) {
					thisGoal = goals.get(j);
					if ((thisBox.getX() == thisGoal.getX()) 
						&& (thisBox.getY() == thisGoal.getY())) {
						// are on top of each other
						int chosenX = thisBox.getX();
						int chosenY = thisBox.getY();
						System.out.println(chosenX);
						System.out.println(chosenY);
						if (chosenX == 240 && chosenY == 320) {
							System.out.println("User is in Single Level");
							currentMode = Mode.SINGLE_PLAYER;
							goals.clear();
							boxes.clear();
							walls.clear();
							lavas.clear();
							moves.clear();
							//LOAD
							loadGame(0);
							replayAllMoves();
							
							return;
						} else if (chosenX == 560 && chosenY == 320) {
							System.out.println("User is in Multiplayer mode");
							currentMode = Mode.MULTIPLAYER;
							goals.clear();
							boxes.clear();
							walls.clear();
							lavas.clear();
							moves.clear();
							movesMultiplayer.clear();
							//LOAD
							loadGame(1);
							replayAllMovesMultiPlayer();
							return;
							
							
						} else if (chosenX == 240 && chosenY == 520) {
							System.out.println("User is in Playing Custom Level");
							currentMode = Mode.CUSTOM_MODE_LOAD;
							goals.clear();
							boxes.clear();
							walls.clear();
							lavas.clear();
							moves.clear();
							isDone = false;
							//Load
							loadGame(2);
							return;
							
						} else if (chosenX == 560 && chosenY == 520) {
							System.out.println("User is in Making Custom Level mode");
							currentMode = Mode.CUSTOM_MODE_MAKING;
							this.createLevelInit = true;
							this.createLevelInitLoad = true;
							goals.clear();
							boxes.clear();
							walls.clear();
							lavas.clear();
							moves.clear();
							score = 0;
							isDone = false;
							//Load
							loadGame(3);
							return;
						}
					}
				}
			}
		}


		// for each box check if it's on top of a goal square
		for (int i = 0; i < num; i++) {
			thisBox = boxes.get(i);
			for (int j = 0; j < num; j++) {
				thisGoal = goals.get(j);
				if ((thisBox.getX() == thisGoal.getX()) 
					&& (thisBox.getY() == thisGoal.getY())) {
					// are on top of each other
					completed += 1;
				}
			} 
		}
		
		if(boxHasLava()) {
			restartLevel();
		}
		
		if (completed == num) {
			// we are done!
			isDone = true;
			System.out.println("Pushed all the boxes in");
			levelNum++;
		}
	}
	
	/**
	 * Move the Board to the next level, whether hard-coded or generated.
	 * 
	 * @post Board will have information for a new level
	 */
	public void moveToNextLevel() {
		if (currentMode == Mode.SINGLE_PLAYER && gameLevels[levelNum] != null) {
			goals.clear();
			boxes.clear();
			walls.clear();
			lavas.clear();
			moves.clear();
			
			readLevel(gameLevels[levelNum]);
			initWorld();
			isDone = false;
			restartLevel();
		} else if (currentMode == Mode.MULTIPLAYER && multigameLevels[levelNum] != null) {
			goals.clear();
			boxes.clear();
			walls.clear();
			lavas.clear();
			moves.clear();
			movesMultiplayer.clear();
			
			readLevel(multigameLevels[levelNum]);
			initWorld();
			isDone = false;
			restartLevel();
		}  else if (currentMode == Mode.GENERATION || currentMode == Mode.GENERATION_MULTI) {
			goals.clear();
			boxes.clear();
			walls.clear();
			lavas.clear();
			moves.clear();
			resetScore();
				
			generateLevels();
			initWorld();
			isDone = false;
			restartLevel();
		}
	}
	
	/**
	 * Checks if all levels in a sequence of hard-coded levels are finished.
	 * 
	 * @return true if all the levels in the sequence (as in the directory) are finished, false otherwise
	 */
	public boolean finishedAllLevels() {
		if ((currentMode == Mode.SINGLE_PLAYER &&
			gameLevels[levelNum] == null) ||
			(currentMode == Mode.MULTIPLAYER &&
			multigameLevels[levelNum] == null)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Resets the score for the game.
	 * Call this when randomly generating a level.
	 */
	public void resetScore() {
		score = 0;
	}
	
	// ================================ Create Your Own Level ==================================
	
	/**
	 * Toggles the createlevelInit field.
	 * 
	 * @post createLevelInit has the opposite boolean value that it had before the method call
	 */
	public void changeCreateLevelInit(){
		if(this.createLevelInit){
			this.createLevelInit = false;
		}
		else{
			this.createLevelInit = true;
		}
	}
	
	/**
	 * Gets the value of createLevelInit
	 * @return
	 */
	public boolean createLevelInit(){
		return this.createLevelInit;
	}
	
	/**
	 * Toggles the status for createlvelInitLoad.
	 * Tells whether or not a custom level is loaded in.
	 * 
	 * @post createLevelInitLoad has the opposite value that it had before this method call
	 */
	public void changeCreateLevelInitLoad(){
		if(this.createLevelInitLoad){
			this.createLevelInitLoad = false;
		}
		else{
			this.createLevelInitLoad = true;
		}
	}
	
	/**
	 * Gets the value of createLevelInitLoad
	 * 
	 * @return boolean value of createLevelInitLoad
	 */
	public boolean createLevelInitLoad(){
		return this.createLevelInitLoad;
	}	
	
	
	// =================================== Back-tracking moves ========================================
	/**
	 * Resets this level.
	 * 
	 * @post player and boxes are returned to their starting positions
	 */
	public void restartLevel() {
		// clear the state of all objects
		goals.clear();
		boxes.clear();
		walls.clear();
		lavas.clear();

		if(currentMode == Mode.MULTIPLAYER || currentMode == Mode.GENERATION_MULTI){
			score = score - movesMultiplayer.size();
		}
		if(currentMode == Mode.SINGLE_PLAYER || currentMode == Mode.GENERATION){
			score = score - moves.size();
		}
		
		moves.clear();
		movesMultiplayer.clear();
		
		// re-initialize
		initWorld();
		isDone = false;
	}
	
	/**
	 * Undoes one step in this level.
	 * 
	 * @post player and boxes are returned to their previous position
	 */
	public void undoStep() {
		// if there's no moves or already finished, don't do anything
		if (moves.isEmpty() || isDone) {
			return;
		}
		
		// clear the state of all objects
		goals.clear();
		boxes.clear();
		walls.clear();
		lavas.clear();
		
		// re-initialize
		initWorld();
		
		// remove the last move from moves
		moves.remove(moves.size() - 1);
		score--;
		
		// execute all the moves in the moves list
		replayAllMoves();
	}
	
	/**
	 * Resets the level and initialLevel to blank levels.
	 */
	public void clearLevels(){
		level = this.initBlankLevel();
		initialLevel = this.initBlankLevel();
	}
	
	// ================================ Saving and Loading Games ===================================
	
	/**
	 * Saves the current game level into the user's directory.
	 * 
	 * @param mode the type of level to be saved (as in Mode enum type class)
	 * @throws IOException
	 */
	public void saveGame(Mode mode) throws IOException {
		File savedGamesDir = null;
		if(mode == Mode.CUSTOM_MODE_TEST){
			if(!isDone){
				System.out.println("Not Solved yet");
				return;
			}
			savedGamesDir = new File ("customModeLevels");
		}
		if(mode == Mode.CUSTOM_MODE_MAKING){
			savedGamesDir = new File("customGamesProgress");
		}
		if(mode == Mode.SINGLE_PLAYER){
			savedGamesDir = new File ("savedGames");
		}
		if(mode == Mode.MULTIPLAYER){
			savedGamesDir = new File("multiSavedGames");
		}
		if (mode == Mode.GENERATION) {
			savedGamesDir = new File("generatedSavedGames");
		}
		if (mode == Mode.GENERATION_MULTI) {
			savedGamesDir = new File("generatedMultiSavedGames");
		}
		
		JFileChooser fileSelector = new JFileChooser(savedGamesDir);
		int status = fileSelector.showSaveDialog(null);
		if ( status == JFileChooser.APPROVE_OPTION) {
			File toBeSaved = fileSelector.getSelectedFile();
		  
			// Initialising Writers
			FileWriter writer = new FileWriter(toBeSaved);
			BufferedWriter bufWriter = new BufferedWriter(writer);
			  
			  
		    if(mode == Mode.MULTIPLAYER || mode == Mode.SINGLE_PLAYER || mode == Mode.GENERATION || mode == Mode.GENERATION_MULTI){
		    	if (mode == Mode.GENERATION) {
		    		// set the level codes to something we can determine as randomly generated levels
		    		// -1 = single player random easy, -2 = single player random medium, -3 = single player random hard
		    		// -4 = multi player random easy, -5 = multi player random medium, -6 = multi player random hard
		    		if (currLvlType.equals(LvlCode.EASY)) {
		    			levelNum = -1;
		    		} else if (currLvlType.equals(LvlCode.MEDIUM)) {
		    			levelNum = -2;
		    		} else if (currLvlType.equals(LvlCode.HARD)) {
		    			levelNum = -3;
		    		} 
		    	} else if (mode == Mode.GENERATION_MULTI) {
		    		if (currLvlType.equals(LvlCode.EASY_MULTI)) {
		    			levelNum = -4;
		    		} else if (currLvlType.equals(LvlCode.MEDIUM_MULTI)) {
		    			levelNum = -5;
		    		} else if (currLvlType.equals(LvlCode.HARD_MULTI)) {
		    			levelNum = -6;
		    		}
		    	}
		    	
		    	// Write the score to the second line
		    	bufWriter.write(Integer.toString(score));
		    	bufWriter.newLine();
		    	// Write isDone to the third Line
		    	if(isDone) bufWriter.write("done");
		    	else bufWriter.write("notdone");
		    	bufWriter.newLine();
		    	// Write the the level
		    	bufWriter.write(Integer.toString(levelNum));
		    	bufWriter.newLine();
		    }
			int i,j;
			

			String rowInitial ="";
			for ( i = 0 ; i < Dimensions.NUM_ROWS ; i++){
				rowInitial = "";
				// Row Initial is Space Separated
				//Can't undo Moves for Multiplayer
				for (j = 0 ; j < Dimensions.NUM_COLS ; j++){
					if(j == 0){
						if(mode == Mode.SINGLE_PLAYER || mode == Mode.CUSTOM_MODE_TEST || mode == Mode.GENERATION)rowInitial += initialLevel[i][j];
						if(mode == Mode.MULTIPLAYER || mode == Mode.CUSTOM_MODE_MAKING || mode == Mode.GENERATION_MULTI) rowInitial += level[i][j];	
					}
					else {
						if(mode == Mode.SINGLE_PLAYER || mode == Mode.CUSTOM_MODE_TEST || mode == Mode.GENERATION) rowInitial += " " + initialLevel [i][j];
						if(mode == Mode.MULTIPLAYER  || mode == Mode.CUSTOM_MODE_MAKING || mode == Mode.GENERATION_MULTI) rowInitial += " " + level[i][j];
					}
				}
				bufWriter.write(rowInitial);
				bufWriter.newLine();
			}
			if(mode == Mode.SINGLE_PLAYER || mode == Mode.GENERATION){
				bufWriter.write("Start Moves");
				bufWriter.newLine();
				// print moves out line by line
				for (String m : moves){	
					bufWriter.write(m);
					bufWriter.newLine();
				}
			}
			if(mode == Mode.MULTIPLAYER || mode == Mode.GENERATION_MULTI){
				bufWriter.write("Start Moves");
				bufWriter.newLine();
				// print moves out line by line
				for (String m : movesMultiplayer){	
					bufWriter.write(m);
					bufWriter.newLine();
				}
			}
		  
			// have to close the file writers
			bufWriter.close();
			writer.close();
			System.out.println("File to Be Saved");
		}
	}
	
	/**
	 * Clears all the items in the level.
	 * 
	 * @post walls, boxes, goals, moves, lavas ArrayLists are emptied and player me is set to null
	 */
	public void clearItems(){
		//We are going to Create new ones based on the new Map
		walls = new ArrayList<Wall>();
		boxes = new ArrayList<Box>();
		goals = new ArrayList<Goal>();
		moves = new ArrayList<String>();
		lavas = new ArrayList<Lava>();
		me = null;
	}
	

	/**
	 * Loads a game form the user's directory.
	 * Opens a dialogue for the user to choose the file to load in. Then loads the data into this board. 
	 * 
	 * @param code 	indicates the type of game code to be inserted
	 * 				0 for single player, 1 for multiplayer, 2 for custom level, 3 for create level
	 */
	public void loadGame(int code){
		File savedGamesDir = null;
		moves.clear();
		movesMultiplayer.clear();
		
		if(code == 0)savedGamesDir = new File ("savedGames");
		else if(code == 1) savedGamesDir = new File ("multiSavedGames");
		else if (code == 2) savedGamesDir = new File("customModeLevels");
		else if(code == 3) savedGamesDir = new File ("customGamesProgress");
		else if (code == 4) savedGamesDir = new File("generatedSavedGames");
		else if (code == 5) savedGamesDir = new File("generatedMultiSavedGames");
		
		JFileChooser fileSelector = new JFileChooser(savedGamesDir);
		int status = fileSelector.showOpenDialog(null);
		if(status == JFileChooser.APPROVE_OPTION){
			System.out.println("File to Be Open");
			File selected = fileSelector.getSelectedFile();
			clearItems(); //new Function Added
			
			//Add Code to Process it 
		    Scanner sc = null;
		    int lineNumber = 1; //STARTING AT 1 not 0 like usual;
		    String currLine = null;
		    String [] fields;
		    int i,j ; //Counters
		    i = 0; 
		    try {
		        sc = new Scanner(selected); 
		        while(sc.hasNextLine()){
		        	currLine = sc.nextLine();
		        	if(currLine.isEmpty())continue;
		        	fields = currLine.split(" ");
		        	//Resetting the score
		        	if(lineNumber == 1 &&(code == 0 || code == 1)) score = Integer.parseInt(currLine);
		        	
		        	//Is the Load Game Completed
		        	if(lineNumber == 2 && (code == 0 || code == 1)){
		        		if(currLine.equals("done"))isDone = true;
		        		else isDone = false;
		        	}
		        	// get the level number for single or multi player
		        	if(lineNumber == 3 &&(code == 0 || code == 1)) this.levelNum = Integer.parseInt(currLine);
		        	// change the mode if this is random level generation
		        	if (levelNum < 0 && levelNum >= -3) {
		        		currentMode = Mode.GENERATION;
		        	} else if (levelNum <= -4 && levelNum >= -6) {
		        		currentMode = Mode.GENERATION_MULTI;
		        	}
		        	
		        	//SCAN THE GRID the grid is stored at different line offsets depending on the mode
		        	if((lineNumber <= 21 && lineNumber >= 4 && (code == 0 || code == 1 )) || ((code == 2||code == 3) && (lineNumber <= 18 && lineNumber >=1))){		        	    
		        		if( i != Dimensions.NUM_ROWS){
		        			j = 0;
		        			for (String s : fields){
		        				if(s.equals("WALL")){
		        					initialLevel[i][j] = Item.WALL;
		        					level[i][j] = Item.WALL;
		        				}
		        				if(s.equals("EMPTY")){
		        					initialLevel[i][j] = Item.EMPTY;
		        					level[i][j] = Item.EMPTY;
		        				}
		        				if(s.equals("BOX")){
		        					initialLevel[i][j] = Item.BOX;
		        					level[i][j] = Item.BOX;
		        					
		        				}
		        				if(s.equals("GOAL")){
		        					initialLevel[i][j] = Item.GOAL;
		        					level[i][j] = Item.GOAL;
		        				}
		        				if(s.equals("PLAYER")){
		        					initialLevel[i][j] = Item.PLAYER;
		        					level[i][j] = Item.PLAYER;
		        				}
		        				if(s.equals("LAVA")) {
		        					initialLevel[i][j] = Item.LAVA;
		        					level[i][j] = Item.LAVA;
		        				}
		        				j++;
		        			}
		        			i++;
		        		}
		        	}
		        	 
		        	//ADDING MOVES only applicable for single player mode
		        	if(lineNumber >= 23 && code == 0){
		        		System.out.println(lineNumber);
		        		this.moves.add(fields[0]);
		        	}
		        	if(lineNumber >= 23 && code == 1){
		        		this.movesMultiplayer.add(currLine);
		        	}
		        	lineNumber ++;
		        }
		        //Finished Scanning the load game file
		         
		        System.out.println(currentMode);
		        sc.close();
		    }
		    catch (FileNotFoundException e) {
		    } finally {
		    	if (sc != null) sc.close();
		    }
		} else {
			currentMode = Mode.LOAD_SCREEN;
			goals.clear();
			boxes.clear();
			walls.clear();
			lavas.clear();
			moves.clear();
			score = 0;
			isDone = false;
			initWorld();
		}
		//Cancel Option is shown
		initWorld();
		System.out.println("Load finished");
		
	}
	
	/**
	 * Replays all the moves for the first player (controlled by arrow keys).
	 * Goes through the moves strings and re-runs them.
	 */
	private void replayAllMoves(){
		System.out.println(moves.size());
		for (String move : moves) {
			System.out.println(move);
			if (move.equals("Up")) {
				updateBox(me, Direction.UP);
				me.move(Direction.UP);
			} else if (move.equals("Down")) {
				updateBox(me, Direction.DOWN);
				me.move(Direction.DOWN);
			} else if (move.equals("Left")) {
				updateBox(me, Direction.LEFT);
				me.move(Direction.LEFT);
			} else if (move.equals("Right")) {
				updateBox(me, Direction.RIGHT);
				me.move(Direction.RIGHT);
			}
		}
	}
	
	/**
	 * Replays all the moves for the second player (controlled by WASD).
	 * Goes through the movesMultiplayer strings and re-runs them.
	 */
	private void replayAllMovesMultiPlayer(){
		System.out.println(movesMultiplayer.size());
		for (String move : movesMultiplayer) {
			System.out.println(move);
			if (move.equals("Me Up")) {
				updateBox(me, Direction.UP);
				me.move(Direction.UP);
			} else if (move.equals("Me Down")) {
				updateBox(me, Direction.DOWN);
				me.move(Direction.DOWN);
			} else if (move.equals("Me Left")) {
				updateBox(me, Direction.LEFT);
				me.move(Direction.LEFT);
			} else if (move.equals("Me Right")) {
				updateBox(me, Direction.RIGHT);
				me.move(Direction.RIGHT);
			}
			
			if (move.equals("Other Up")) {
				updateBox(otherPlayer, Direction.UP);
				otherPlayer.move(Direction.UP);
			} else if (move.equals("Other Down")) {
				updateBox(otherPlayer, Direction.DOWN);
				otherPlayer.move(Direction.DOWN);
			} else if (move.equals("Other Left")) {
				updateBox(otherPlayer, Direction.LEFT);
				otherPlayer.move(Direction.LEFT);
			} else if (move.equals("Other Right")) {
				updateBox(otherPlayer, Direction.RIGHT);
				otherPlayer.move(Direction.RIGHT);
			}
		}
	}

	/**
	 * Initializes an empty level.
	 * Call this before hardcoding solutions / placing objects in places / telling algos to put items on places.
	 * 
	 * @return	Item[Dimensions.NUM_COLS][Dimensions.NUM_COLS] with every item initialized to Item.EMPTY
	 */
	private Item[][] initBlankLevel() {
		Item[][] blank = new Item[Dimensions.NUM_ROWS][Dimensions.NUM_COLS];
		int row, col;	// counters
		
		// initialize everything to empty
		for (row = 0; row < Dimensions.NUM_ROWS; row++) {
			for (col = 0; col < Dimensions.NUM_COLS; col++) {
				blank[row][col] = Item.EMPTY;
			}
		}
		
		return blank;
	}
	
	/**
	 * get a level from the text file
	 * @param fileName  name of the text file
	 * @return  gameLevel   of type String[][]. Use for initialize Items in level later
	 */
	public void readLevel(String fileName) {
		String temp = null;
		System.out.println(currentMode);
		//Current Mode is null only when the board is being initially constructed
		if(currentMode == null || currentMode == Mode.START_SCREEN){
			temp = "./startScreen/";
		}
		
		if (currentMode == Mode.LOAD_SCREEN) {
			temp = "./loadScreen/";
		}
		
		if(currentMode == Mode.SINGLE_PLAYER){
			temp = "./singlePlayerLevels/";	
		}
		
		if(currentMode == Mode.MULTIPLAYER){
			temp = "./multiPlayerLevels/";
		}
		
		if(currentMode == Mode.CUSTOM_MODE_LOAD){
			temp = "./customModeLevels/";
		}
			 
		String filePath = temp.concat(fileName);
		System.out.println(filePath);	
		Scanner sc = null;
		String currentLine = null;
		String[] data = null;
		int i = 0;
		int j = 0; //counters
		
		try {
			sc = new Scanner(new FileReader(filePath));
			System.out.println(sc);
			while(sc.hasNextLine()) {
				currentLine = sc.nextLine();
				if(currentLine.isEmpty()) {
					continue;
				}
				
				data = currentLine.split(" ");
				
				if(i != Dimensions.NUM_ROWS) {
					j = 0;
					for(String s : data) {
						if(s.equals("WALL")) {
							level[i][j] = Item.WALL;
							initialLevel[i][j] = Item.WALL;
						}
						if(s.equals("EMPTY")) {
							level[i][j] = Item.EMPTY;
							initialLevel[i][j] = Item.EMPTY;
						}
						if(s.equals("BOX")) {
							level[i][j] = Item.BOX;
							initialLevel[i][j] = Item.BOX;
						}
						
						if(s.equals("GOAL")) {
							level[i][j] = Item.GOAL;
							initialLevel[i][j] = Item.GOAL;
						}
						if(s.equals("PLAYER")) {
							level[i][j] = Item.PLAYER;
							initialLevel[i][j] = Item.PLAYER;
						}
						if(s.equals("LAVA")) {
							level[i][j] = Item.LAVA;
							initialLevel[i][j] = Item.LAVA;
						}
						j++;
					}
					i++;
				}
			}	
		} catch (FileNotFoundException e) {
			System.out.println("No level to play");
		} finally {
			if(sc != null) {
				sc.close();
			}
		}
	}

	/**
	 * get all the file for a directory
	 * @param curDir  current directory
	 * @return a string of all game levels available
	 */
	public String[] getAllFiles(File curDir) {
		String[] gameLevels = new String[40]; // assume only have 40 levels first
		int i = 0;
		
        File[] filesList = curDir.listFiles();
        for(File f : filesList){
            if(f.isDirectory())
            	continue;
            if(f.isFile()){
                gameLevels[i] = f.getName();
                i++;
            }
        }
        
        return gameLevels;
    }

	/**
	 * Get all the game levels.
	 * 
	 * @return an array containing all the available game levels as a String.
	 */
	public String[] getGameLevels() {
		return gameLevels;
	}

	/**
	 * Sets up the starting level state when a load file is used.
	 */
	public void setUpForTest() {
		int i,j;
		for (i = 0 ; i < Dimensions.NUM_ROWS ; i++){
			for (j = 0 ; j < Dimensions.NUM_COLS ; j++){
				initialLevel[i][j] = level[i][j];
			}
		}
		
	}
	
	// ================================ Randomly Generate Levels ============================
	/**
	 * Generates a random level of the type specified in currLvlType.
	 * Calls the generator.
	 * 
	 * @pre currLvlType contains the value of the type of code that is to be generated (use the setter)
	 * @post level and initialLevel are initialized to the same random level
	 */
	public void generateLevels() {
		if (currLvlType.equals(LvlCode.EASY)) {
			level = generator.getLevel(LvlCode.EASY);
		} else if (currLvlType.equals(LvlCode.MEDIUM)) {
			level = generator.getLevel(LvlCode.MEDIUM);
		} else if (currLvlType.equals(LvlCode.HARD)) {
			level = generator.getLevel(LvlCode.HARD);
		} else if (currLvlType.equals(LvlCode.EASY_MULTI)) {
			level = generator.getLevel(LvlCode.EASY_MULTI);
		} else if (currLvlType.equals(LvlCode.MEDIUM_MULTI)) {
			level = generator.getLevel(LvlCode.MEDIUM_MULTI);
		} else if (currLvlType.equals(LvlCode.HARD_MULTI)) {
			level = generator.getLevel(LvlCode.HARD_MULTI);
		}
		
		initialLevel = level.clone();
		
		return;
	}
	
	/**
	 * Counts the number of goals in this level.
	 * 
	 * @return the number of goals in the level
	 */
	public int numberOfGoals(){
		int i , j;
		int numGoals =0;
		for (i = 0 ; i < Dimensions.NUM_ROWS ; i++){
			for (j = 0 ; j < Dimensions.NUM_COLS ; j++){
				if(level[i][j].equals(Item.GOAL))numGoals ++;;
			}
		}
		return numGoals;
	}
	
	/**
	 * Counts the number of boxes in this level.
	 * 
	 * @return the number of boxes in the level
	 */
	public int numberOfBoxes(){
		int i , j;
		int numBoxes =0;
		for (i = 0 ; i < Dimensions.NUM_ROWS ; i++){
			for (j = 0 ; j < Dimensions.NUM_COLS ; j++){
				if(level[i][j].equals(Item.BOX))numBoxes ++;;
			}
		}
		return numBoxes;
	}
	
	/**
	 * Counts the number of players in this level.
	 * 
	 * @return the number of players in the level.
	 */
	public int numberOfPlayers(){
		int i , j;
		int numPlayers =0;
		for (i = 0 ; i < Dimensions.NUM_ROWS ; i++){
			for (j = 0 ; j < Dimensions.NUM_COLS ; j++){
				if(level[i][j].equals(Item.PLAYER))numPlayers ++;;
			}
		}
		return numPlayers;	
	}
	

}
