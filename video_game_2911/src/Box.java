import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;

/**
 * This class represents a Box that the Player must push to Goal squares.
 * Constructor sets the image for this Box, and this Box can be moved.
 * 
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public class Box extends Actor {

	/**
	 * Modified constructor for Box to load the image for this instance.
	 * 
	 * @param x	x-coordinate of bottom-right corner of this tile
	 * @param y y-coordinate of bottom-right corner of this tile
	 */
	public Box(int x, int y) {
        super(x, y);

        URL loc = this.getClass().getResource("icons/pikachu.png");
        ImageIcon icon = new ImageIcon(loc);
        Image image = icon.getImage();
        setImage(image);
    }

	/**
	 * Moves the Box by the given parameters.
	 * 
	 * @param x	amount in the x-direction to move Player (can be pos, neg, or 0)
	 * @param y amount in the y-direction to move Player (can be pos, neg, or 0)
	 */
    public void move(int x, int y) {
        int newX = getX() + x;
        int newY = getY() + y;
        setX(newX);
        setY(newY);
    }
	
}
