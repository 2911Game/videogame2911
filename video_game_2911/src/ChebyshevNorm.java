/**
 * Implements the Chebyshev distance norm, i.e. p_infinity metric.
 *
 */
public class ChebyshevNorm implements Norm {

	@Override
	public double calcNorm(Coordinate a, Coordinate b) {
		int changeX = Math.abs(a.getX() - b.getX());
		int changeY = Math.abs(a.getY() - b.getY());
		
		if (changeX > changeY) {
			return changeX;
		} else {
			return changeY;
		}
	}

}
