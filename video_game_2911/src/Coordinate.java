/**
 * Represents an (x,y) coordinate pair of an Item[][] matrix.
 * 
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public class Coordinate {

	private int x;
	private int y;
	
	//constructors
	/**
	 * 2-argument constructor for this Coordinate pair.
	 * 
	 * @pre caller's responsibility to make sure this doesn't go out of bounds of the Dimensions
	 * @param x	x-coordinate
	 * @param y	y-coordinate
	 */
	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * No-argument constructor for a Coordinate pair.
	 * Will have to set x,y values using setters.
	 * 
	 * @post responsibility of the caller to not call getX or getY until setX or setY is called
	 */
	public Coordinate() {
		
	}
	
	// getters and setters
	
	/**
	 * Getter for the x-coordinate.
	 * 
	 * @return	x-coordinate
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Setter for the x-coordinate.
	 * 
	 * @param x integer value to set the x-coordinate to
	 */
	public void setX(int x) {
		this.x = x;
	}
	
	/**
	 * Getter for the y-coordinate.
	 * 
	 * @return y-coordinate
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Setter for the y-coordinate.
	 * 
	 * @param y integer value to set the y-coordinate to
	 */
	public void setY(int y) {
		this.y = y;
	}
	
	/**
	 * Represents Coordinate as a simple "(x,y)" String
	 * 
	 * return String representation of Coordinate
	 */
	@Override
	public String toString() {
		return "(" + new Integer(x).toString() + ", " + new Integer(y).toString() + ")";
	}
	
}
