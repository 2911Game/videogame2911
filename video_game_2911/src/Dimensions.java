
/**
 * This class holds the global variables for the dimensions of the game.
 * 
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public class Dimensions {
	public final static int SPACE = 40;	// size of each Item image is 40 x 40 px 
	public final static int NUM_ROWS = 18;
	public final static int NUM_COLS = 21;
}
