/**
 * Enum type for the location of a collision relative to the main object.
 * 
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public enum Direction {
	UP, DOWN, LEFT, RIGHT
}
