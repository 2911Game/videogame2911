import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * This class acts as the view for the game board.
 * It draws out each individual square into a JPanel.
 * 
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public class DrawBoard extends JPanel {
	
	private Board board;
	
	/**
	 * Constructor for DrawBoard.
	 * Sets the dimensions for this panel, preferred size, and
	 * stores the parameter board in its field.
	 * 
	 * @pre board is the same object as in the field of Game that composes this class
	 * @param board	instance of the Board that represents the data of the game
	 */
	public DrawBoard (Board board) {
		Dimension dimensions = new Dimension(Dimensions.SPACE * Dimensions.NUM_COLS, Dimensions.SPACE * Dimensions.NUM_ROWS);
		setPreferredSize(dimensions);
		setFocusable(false);
		this.board = board;
	}
	
	/**
	 * Draws the Item's onto the rectangle and updates the view
	 * 
	 * @param g the graphics object to draw
	 */
	public void drawWorld (Graphics g) {
		
		//Adding Button
		if (board.getMode() == Mode.CUSTOM_MODE_MAKING){
           return;
		}
		
		g.setColor(new Color(139, 219, 104));		// set working color (green)
		g.fillRect(0, 0, getWidth(), getHeight());	// fill the background of this rectangle
		
		if (board.finishedAllLevels() && !(board.getMode() == Mode.SPLASH_SCREEN ||
				board.getMode() == Mode.START_SCREEN || board.getMode() == Mode.LOAD_SCREEN ||
				board.getMode() == Mode.CUSTOM_MODE_TEST || board.getMode() == Mode.CUSTOM_MODE_MAKING)) {
			URL gameComplete = this.getClass().getResource("icons/gameComplete.gif");
            Image imageGameComplete = (new ImageIcon(gameComplete)).getImage();
            g.drawImage(imageGameComplete, 4 * Dimensions.SPACE, 30, this);
            return;
		}
		
		// Finished level screen
		if (board.isDone() && !(board.getMode() == Mode.SPLASH_SCREEN ||
				board.getMode() == Mode.START_SCREEN || board.getMode() == Mode.LOAD_SCREEN ||
				board.getMode() == Mode.CUSTOM_MODE_TEST || board.getMode() == Mode.CUSTOM_MODE_MAKING)) {
			URL finishedLevel = this.getClass().getResource("icons/finishedLevel.gif");
            Image imageFinishedLevel = (new ImageIcon(finishedLevel)).getImage();
            g.drawImage(imageFinishedLevel, 4 * Dimensions.SPACE, 30, this);
            return;
		}
		
		// Finished level screen for 
		if (board.isDone() && board.getMode() == Mode.CUSTOM_MODE_TEST) {
			URL finishedLevel = this.getClass().getResource("icons/finishedLevelCustom.gif");
            Image imageFinishedLevel = (new ImageIcon(finishedLevel)).getImage();
            g.drawImage(imageFinishedLevel, 4 * Dimensions.SPACE, 30, this);
            return;
		}
		
		ArrayList<Actor> world = board.getAllItems();
		
		// draw the images
		int i;
		for (i = 0; i < world.size(); i++) {
			Actor item = world.get(i);
			if(item == null)continue;

			g.drawImage(item.getImage(), item.getX(), item.getY(), this);
		}

		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("fonts/Pokemon_GB.ttf");
		Font font = null;
		try {
			font = Font.createFont(Font.TRUETYPE_FONT, inputStream);
			Font sizedFont = font.deriveFont(Font.BOLD, 16f);
			g.setFont(sizedFont);
		} catch (FontFormatException | IOException e1) {
			// do nothing and default to default font
		}
		
		if (board.getMode() == Mode.START_SCREEN) {
        	URL bannerMain = this.getClass().getResource("icons/bannerMain.png");
            Image imageBannerMain = (new ImageIcon(bannerMain)).getImage();
            
        	URL singlePlayer = this.getClass().getResource("icons/single_player.png");
        	Image imageSinglePlayer = (new ImageIcon(singlePlayer)).getImage();
        	
        	URL multiPlayer = this.getClass().getResource("icons/multi_player.png");
        	Image imageMultiPlayer = (new ImageIcon(multiPlayer)).getImage();
        	
        	URL createLevel = this.getClass().getResource("icons/create_level.png");
        	Image imageCreateLevel = (new ImageIcon(createLevel)).getImage();
        	
        	URL loadLevel = this.getClass().getResource("icons/load_level.png");
        	Image imageLoadLevel = (new ImageIcon(loadLevel)).getImage();
        	
        	g.drawImage(imageBannerMain, 4 * Dimensions.SPACE, 1 * Dimensions.SPACE, this);
        	g.drawImage(imageSinglePlayer, 5 * Dimensions.SPACE, 6 * Dimensions.SPACE, this);
        	g.drawImage(imageMultiPlayer, 13 * Dimensions.SPACE,  6 * Dimensions.SPACE, this);
        	g.drawImage(imageCreateLevel, 5 * Dimensions.SPACE, 11 * Dimensions.SPACE, this);
        	g.drawImage(imageLoadLevel, 13 * Dimensions.SPACE, 11 * Dimensions.SPACE, this);
		}
		
		if (board.getMode() == Mode.LOAD_SCREEN) {
			URL bannerMain = this.getClass().getResource("icons/bannerMain.png");
            Image imageBannerMain = (new ImageIcon(bannerMain)).getImage();
			
			URL single = this.getClass().getResource("icons/loadSingle.png");
            Image imageSingle = (new ImageIcon(single)).getImage();
            
        	URL multi = this.getClass().getResource("icons/loadMulti.png");
        	Image imageMulti = (new ImageIcon(multi)).getImage();
        	
        	URL custom = this.getClass().getResource("icons/loadCustom.png");
        	Image imageCustom = (new ImageIcon(custom)).getImage();
        	
        	URL customEdit = this.getClass().getResource("icons/loadCustomEdit.png");
        	Image imageCustomEdit = (new ImageIcon(customEdit)).getImage();
        	
        	g.drawImage(imageBannerMain, 4 * Dimensions.SPACE, 1 * Dimensions.SPACE, this);
        	g.drawImage(imageSingle, 5 * Dimensions.SPACE, 6 * Dimensions.SPACE, this);
        	g.drawImage(imageMulti, 13 * Dimensions.SPACE,  6 * Dimensions.SPACE, this);
        	g.drawImage(imageCustom, 5 * Dimensions.SPACE, 11 * Dimensions.SPACE, this);
        	g.drawImage(imageCustomEdit, 13 * Dimensions.SPACE, 11 * Dimensions.SPACE, this);
		}
		
		if (!board.isDone()) {
			g.setColor(new Color(0, 0, 0));
	        
		} else {
			g.setColor(new Color(0, 0, 0));
			if (board.getMode() == Mode.START_SCREEN || board.getMode() == Mode.LOAD_SCREEN || board.getMode() == Mode.GENERATION || board.getMode() == Mode.GENERATION_MULTI) {
				
			}
		}
	}
	
	/**
	 * Overrides JComponent's paint method to draw all the Item's
	 * 
	 * @see JComponent
	 * @param g	Graphics object to be painted
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		drawWorld(g);
	}
	

	/**
	 * Effectively Removes All the Buttons
	 * 
	 * @post buttons will be removed from the GUI
	 */
	public void removeAllButtons(){
		this.removeAll();
	}
	
}
