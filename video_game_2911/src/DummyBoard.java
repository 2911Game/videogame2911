import java.util.ArrayList;

/**
 * Subclass of Board that we can use to generate random levels.
 * Has more public setters and a modified constructor for LevelGenerator use.
 * Also doesn't worry about most of the functionality of Board as it's not needed
 * to generate levels.
 *
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public class DummyBoard extends Board {

	/**
	 * Constructor for DummyBoard that initializes a board based on an Item[][] matrix.
	 * 
	 * @pre Item[][] has dimensions as stated in Dimensions class of constants.
	 * @param randomLevel
	 */
	public DummyBoard(Item[][] randomLevel) {
		super();
		
		// initialize the board with this
		level = randomLevel.clone();
		
		setMode(Mode.SINGLE_PLAYER);
		
		// reset walls, etc. that super constructor set
		clearItems();
		
		initWorld();
	}
	
	
	/**
	 * Moves player one (controlled by arrow keys) in the given direction.
	 * Only one player is needed to generate levels, so no option is given to choose a Player.
	 * 
	 * @param dir direction to move player in
	 */
	public void movePlayer(Direction dir) {
		int me = 0;
		
		if (hasWallCollisionWrap(me, dir)) {
			return;
		} else if (hasImmovableBoxWrap(me, dir)) {
			return;
		} else if (hasEdgeCollisionWrap(me, dir)) {
			return;
		} else {

			moveBox(me, dir);
			movePlayer(me, dir);
		}
		
	}
	
	/**
	 * Returns the (x, y) (in terms of Item[][] matrix) location of the first player.
	 * 
	 * @return	Coordinate of the first player
	 */
	public Coordinate getPlayerPos() {
		int playerNum = 0;
		
		return new Coordinate((getPlayerXPos(playerNum) / Dimensions.SPACE), (getPlayerYPos(playerNum) / Dimensions.SPACE));
	}
	
}
