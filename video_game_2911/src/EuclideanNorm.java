/**
 * Implements Norm as a Euclidean norm. i.e. p_2 metric
 * Will always prefer right angles for maximum distance,
 * as the shortest Euclidean distance is on the straight line.
 *
 */
public class EuclideanNorm implements Norm {

	@Override
	public double calcNorm(Coordinate a, Coordinate b) {
		double changeX = (double)(a.getX() - b.getX());
		double changeY = (double)(a.getY() - b.getY());
		
		return Math.sqrt(changeX * changeX + changeY * changeY);
	}

}
