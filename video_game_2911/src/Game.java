import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.URL;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * The start of the video game.
 * Sets up the JFrame.
 * Initial structure given by walkthrough linked below.
 * Walkthrough didn't have a Game class, didn't use Enum types, and the levels were set differently.
 * Functions had to be decoupled. All extensions are our own. 
 * This class is part of the View of the MVC Architecture pattern.
 * 
 * @see http://zetcode.com/tutorials/javagamestutorial/sokoban/
 * 
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public class Game extends JFrame {
	
	private Board board;
	private DrawBoard drawBoard;
	private Instructions instructions;
	private SplashScreen splashScreen;
	private boolean generateLevel = false;
	
	// constructor
	/**
	 * Constructor for Game.
	 * 
	 * Creates the window for the video game and sets properties of this window.
	 * Initializes fields board, gameBoard, instructions and adds them to this window.
	 * 
	 * @throws IOException
	 * @post board mode set to SPLASH_SCREEN
	 * @post board is drawn
	 * @post instructions added to the board
	 * @post things are resized, window is centered, and key input is linked to the application
	 */
	public Game() throws IOException {
		setLayout(new BorderLayout());
		
		setTitle("Pikachu Push!");
		setDefaultCloseOperation(EXIT_ON_CLOSE);	// closes if we click the Close button
		setResizable(false);
		
		String OS = System.getProperty("os.name").toLowerCase();
		if (OS.indexOf("mac") >= 0) {
			Runtime.getRuntime().exec("defaults write NSGlobalDomain ApplePressAndHoldEnabled -bool false");
			
			Runtime.getRuntime().addShutdownHook(new Thread() {
				public void run() {
					try {
						Runtime.getRuntime().exec("defaults write NSGlobalDomain ApplePressAndHoldEnabled -bool true");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
		}
		// initialize the model/data for the game and set the default mode
		board = new Board();
		board.setMode(Mode.SPLASH_SCREEN);
		
		drawBoard = new DrawBoard(board);
		
		splashScreen = new SplashScreen();
		add(splashScreen, BorderLayout.CENTER);
		

		instructions = new Instructions(board);
		
		// resizes things so they fit nicely
		pack();
		
		addKeyListener(new MoveAdapter());			// abstract inner class for moving Player using the arrow keys
		setLocationRelativeTo(null);				// Centers the window
		setVisible(true);							// Make the frame visible
	}
	
	/**
	 * Main function for the game.
	 * Initializes an instance of game, whose constructor starts the game process.
	 * Sets up a directory within src for saving files.
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException  {
		Game game = new Game();
		setUpDirectory(); 					// Sets up a directory within src for saving files if it doesn't exist
	}
	

	/**
	 * Checks if a savedGames directory exists in the source file.
	 * Sets up one if it has not been created.
	 * 
	 * @throws IOException
	 */
	private static void setUpDirectory() throws IOException{
		Path directory = Paths.get("savedGames");
		System.out.println(directory);
		try {
			// There is an optional second arg that specifies flags
			// Might need this
			Files.createDirectory(directory);
			System.out.println("Directory Created");
		} catch (FileAlreadyExistsException f) {
			System.out.println("Directory Exists");
		}
	}
	
	/**
	 * Changes the game display to the menu screen when the user presses the enter key.
	 * 
	 * @post mode is now START_SCREEN
	 * @post application shows the start screen with instructions changed appropriately
	 */
	private void endSplashScreen() {
		board.setMode(Mode.START_SCREEN);
		remove(splashScreen);
		repaint();
		add(drawBoard, BorderLayout.WEST);
		add(instructions, BorderLayout.EAST);
		
		pack();
		repaint();
	}
	
	/**
	 * Performs the functionality for the custom building mode.
	 * Sets up images for each item on a level during custom builder mode.
	 * Determines what item has been requested, creates the item and uploads its respective image.
	 * 
	 * @post changes the layout of DrawBoard to a GridLayout to custom the modes 
	 */
	public void customBuilds(){
	
		drawBoard.setLayout(new GridLayout(Dimensions.NUM_ROWS,Dimensions.NUM_COLS,0,0));
		int i,j;
		for (j = 0 ; j < Dimensions.NUM_ROWS ; j++) {
			for (i = 0 ; i < Dimensions.NUM_COLS ; i++) {
				String name = Integer.toString(j) + " " + Integer.toString(i);
				JButton jb = new JButton();
				jb.setName(name);
				Dimension buttonDimensions = new Dimension (Dimensions.SPACE, Dimensions.SPACE);
				jb.setPreferredSize(buttonDimensions);
				jb.setMargin(new Insets(0,0,0,0));
				jb.setBackground(new Color(139, 219, 104));
				
				Item currentItem = board.whatsInGrid(j, i);
				if(currentItem == Item.WALL){
					URL loc = this.getClass().getResource("icons/wall.png");
					ImageIcon icon = new ImageIcon(loc);
					jb.setContentAreaFilled(false);
					jb.setIcon(icon);					
				}
				else if(currentItem == Item.GOAL){
					URL loc = this.getClass().getResource("icons/grass_patch.png");
					ImageIcon icon = new ImageIcon(loc);
					jb.setContentAreaFilled(false);
					jb.setIcon(icon);					
				}
				else if(currentItem == Item.PLAYER){
					URL loc = this.getClass().getResource("icons/ash.png");
					ImageIcon icon = new ImageIcon(loc);
					jb.setContentAreaFilled(false);
					jb.setIcon(icon);					
				}
				else if(currentItem == Item.BOX){
					URL loc = this.getClass().getResource("icons/pikachu.png");
					ImageIcon icon = new ImageIcon(loc);
					jb.setContentAreaFilled(false);
					jb.setIcon(icon);					
				}
				
				jb.addActionListener(new ActionListener() {
					
					/**
					 * Determines the location of where the user has requested and
					 * what the item that has been requested is.
					 * 
					 * @param e mouse-click event
					 */
					@Override
					public void actionPerformed(ActionEvent e) {
				
						String coords [] = jb.getName().split(" ");
						int x = Integer.parseInt(coords[0]);
						int y = Integer.parseInt(coords[1]);

						Item statusSquare = board.whatsInGrid(x, y);
						String[] options;
						int response;
						
						if (statusSquare == Item.EMPTY) {
							options = new String [] {"Add Wall", "Add Pikachu" ,"Add Player" ,"Add Goal" , "Add Acid", "Do Nothing"};
							response = JOptionPane.showOptionDialog(null, "Please Select an Option", "Custom Map Builder" , 
                                                                    JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,						
                                                                    options, options[5]);
						} else {
							options = new String [] {"Change to Wall", "Change to Pikachu", "Change to Player", "Change to Goal", "Change to Acid",
													 "Remove Item", "Do Nothing"};
							response = JOptionPane.showOptionDialog(null, "Please Select an Option", "Custom Map Builder", 
	                                                                JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,						
	                                                                options, options[5]);
						}
						
						if (response == 0) {
							URL loc = this.getClass().getResource("icons/wall.png");
							ImageIcon icon = new ImageIcon(loc);
							jb.setContentAreaFilled(false);
							jb.setIcon(icon);
							board.setGrid(x, y, Item.WALL);
						} else if (response == 1) {
							URL loc = this.getClass().getResource("icons/pikachu.png");
							ImageIcon icon = new ImageIcon(loc);
							jb.setContentAreaFilled(false);
							jb.setIcon(icon);
							board.setGrid(x, y, Item.BOX);
						} else if (response == 2) {
							URL loc = this.getClass().getResource("icons/ash.png");
							ImageIcon icon = new ImageIcon(loc);
							jb.setContentAreaFilled(false);
							jb.setIcon(icon);
							board.setGrid(x, y, Item.PLAYER);
						} else if (response == 3) {
							URL loc = this.getClass().getResource("icons/grass_patch.png");
							ImageIcon icon = new ImageIcon(loc);
							jb.setContentAreaFilled(false);
							jb.setIcon(icon);
							board.setGrid(x, y, Item.GOAL);
						} else if (response == 4) {
							URL loc = this.getClass().getResource("icons/lava.png");
							ImageIcon icon = new ImageIcon(loc);
							jb.setContentAreaFilled(false);
							jb.setIcon(icon);
							board.setGrid(x, y, Item.LAVA);
						} else if (response == 5) {
							jb.setIcon(null);
							board.setGrid(x, y, Item.EMPTY);
						} 
						System.out.println("Response is " + response);			
						
						// If the square is occupied Change it or Remove						
						System.out.println(requestFocusInWindow());
					}
				});
				drawBoard.add(jb);
				System.out.println(j);
			}
		}
		drawBoard.validate();
		
	}
	
	/**
	 * Responds to key inputs of the user.
	 * Calls all public methods of Board needed to request information (i.e. about Collisions)
	 * and set the changes in Board that are required.
	 * Acts as the 'Controller' of the MVC Architecture
	 *
	 * @post board will be updated with the responses to the key inputs
	 * @post the GUI will be updated in response to the key inputs
	 */
	class MoveAdapter extends KeyAdapter {
		
		/**
		 * Respond to key-pressed events, i.e. move the Player around in response to arrow keys.
		 * Also moves Boxes if the Player collides with the Box and there isn't another Box
		 * or wall behind that Box preventing it from moving.
		 * 
		 * @param e	the KeyEvent (i.e. arrow key pressed) that was created after arrow key pressed
		 */
		@Override
		public void keyPressed(KeyEvent e) {
			System.out.println("Hi");
			int key = e.getKeyCode();
			
			// get information from the board
			Mode currentMode = board.getMode();
			
			// respond to general system command shortcuts
			if (key == KeyEvent.VK_ENTER) {
				System.out.println(currentMode);
				System.out.println(board.isDone());
				if (currentMode == Mode.SPLASH_SCREEN) {
					endSplashScreen();
					System.out.println("derp");
				}  else if (currentMode == Mode.CUSTOM_MODE_LOAD && board.isDone()) {
					board.setMode(Mode.START_SCREEN);
					board.readLevel("StartScreen");
					board.clearItems();
					board.setLevelNum(0);
					board.initWorld();
				} else if (board.isDone() && !board.finishedAllLevels()) {
					board.moveToNextLevel();
				} else if (board.finishedAllLevels()) {
					board.setMode(Mode.START_SCREEN);
					board.readLevel("StartScreen");
					board.clearItems();
					board.setLevelNum(0);
					board.initWorld();
				}
			} else if (key == KeyEvent.VK_Q) {
				System.exit(0);
			} else if (key == KeyEvent.VK_R && currentMode != Mode.START_SCREEN && currentMode != Mode.SPLASH_SCREEN) {
				board.restartLevel();
			} else if (key == KeyEvent.VK_Z && (currentMode ==  Mode.SINGLE_PLAYER ||currentMode == Mode.CUSTOM_MODE_LOAD || currentMode == Mode.GENERATION)) {
				board.undoStep();	// undoing a move
			} else if (key == KeyEvent.VK_S && (currentMode ==  Mode.SINGLE_PLAYER || currentMode == Mode.CUSTOM_MODE_TEST 
					|| currentMode == Mode.CUSTOM_MODE_MAKING || currentMode == Mode.GENERATION)) {
				// saving a game
				try {
					board.saveGame(board.getMode());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			
			} else if(key == KeyEvent.VK_C && (currentMode == Mode.MULTIPLAYER || currentMode == Mode.GENERATION_MULTI)){
				try {
					board.saveGame(board.getMode());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			} else if (key == KeyEvent.VK_ESCAPE){
				if (currentMode == Mode.SINGLE_PLAYER ||currentMode == Mode.CUSTOM_MODE_LOAD || currentMode == Mode.MULTIPLAYER || currentMode == Mode.CUSTOM_MODE_MAKING 
						|| currentMode == Mode.CUSTOM_MODE_TEST || currentMode == Mode.LOAD_SCREEN || currentMode == Mode.GENERATION || currentMode == Mode.GENERATION_MULTI) {
					
					if (currentMode == Mode.CUSTOM_MODE_MAKING){
						drawBoard.removeAllButtons();
					}
					System.out.println(currentMode);
					board.setMode(Mode.START_SCREEN);
					board.readLevel("StartScreen");
					board.clearItems();
					board.setLevelNum(0);
					board.initWorld();
					
					
				}
			} else if ((key == KeyEvent.VK_E || key == KeyEvent.VK_M || key == KeyEvent.VK_H) 
					&& (currentMode == Mode.GENERATION || currentMode == Mode.SINGLE_PLAYER)
					&& !generateLevel) {
				
				board.setMode(Mode.GENERATION);
				
				generateLevel = true;
				
				if (key == KeyEvent.VK_E) {
					board.setLevelType(LvlCode.EASY);
				} else if (key == KeyEvent.VK_M) {
					board.setLevelType(LvlCode.MEDIUM);
				} else if (key == KeyEvent.VK_H) {
					board.setLevelType(LvlCode.HARD);
				}
				
				board.clearItems();
				board.clearLevels();
				board.resetScore();
				board.generateLevels();
				board.initWorld();
				
				generateLevel = false;
			} else if ((key == KeyEvent.VK_E || key == KeyEvent.VK_M || key == KeyEvent.VK_H) 
					&& (currentMode == Mode.GENERATION_MULTI || currentMode == Mode.MULTIPLAYER)) {
				board.setMode(Mode.GENERATION_MULTI);
				
				if (key == KeyEvent.VK_E) {
					board.setLevelType(LvlCode.EASY_MULTI);
				} else if (key == KeyEvent.VK_M) {
					board.setLevelType(LvlCode.MEDIUM_MULTI);
				} else if (key == KeyEvent.VK_H) {
					board.setLevelType(LvlCode.HARD_MULTI);
				}
				
				board.clearItems();
				board.clearLevels();
				board.resetScore();
				board.generateLevels();
				board.initWorld();
			}
			
			if (!board.isDone()) {
				// register and respond to arrow key (or WASD for multiplayer) inputs
				int playerNum = -1;
				Direction direction = null;
				
				if (key == KeyEvent.VK_UP) {
					direction = Direction.UP;
					playerNum = board.getMe();
				} else if (key == KeyEvent.VK_DOWN) {
					direction = Direction.DOWN;
					playerNum = board.getMe();
				} else if (key == KeyEvent.VK_LEFT) {
					direction = Direction.LEFT;
					playerNum = board.getMe();
				} else if (key == KeyEvent.VK_RIGHT) {
					direction = Direction.RIGHT;
					playerNum = board.getMe();
				} else if (key == KeyEvent.VK_W && (currentMode == Mode.MULTIPLAYER || currentMode == Mode.GENERATION_MULTI)) {
					direction = Direction.UP;
					playerNum = board.getOtherPlayer();
				} else if (key == KeyEvent.VK_S && (currentMode == Mode.MULTIPLAYER || currentMode == Mode.GENERATION_MULTI)) {
					direction = Direction.DOWN;
					playerNum = board.getOtherPlayer();
				} else if (key == KeyEvent.VK_A && (currentMode == Mode.MULTIPLAYER || currentMode == Mode.GENERATION_MULTI)) {
					direction = Direction.LEFT;
					playerNum = board.getOtherPlayer();
				} else if (key == KeyEvent.VK_D && (currentMode == Mode.MULTIPLAYER || currentMode == Mode.GENERATION_MULTI)) {
					direction = Direction.RIGHT;
					playerNum = board.getOtherPlayer();
				}
				
				if (playerNum != -1) {
					if (board.hasWallCollisionWrap(playerNum, direction)) {
						System.out.println("wall collision");
						return;
					} else if(board.hasPlayerCollisionWrap(playerNum, direction)) {
						System.out.println("player collision");
						return;
					} else if (board.hasImmovableBoxWrap(playerNum, direction)) {
						System.out.println("immovable box collision");
						return;
					} else if (board.hasEdgeCollisionWrap(playerNum, direction)) {
						System.out.println("map edge collision");
						return;
					} else {
						if (currentMode == Mode.SINGLE_PLAYER || currentMode == Mode.CUSTOM_MODE_LOAD || currentMode == Mode.GENERATION) {
							System.out.println("hello");
							if (direction.equals(Direction.UP)) {
								board.addMove("Up");
							} else if (direction.equals(Direction.DOWN)) {
								board.addMove("Down");
							} else if (direction.equals(Direction.LEFT)) {
								board.addMove("Left");
							} else if (direction.equals(Direction.RIGHT)) {
								board.addMove("Right");
							}
						}
						
						if( currentMode == Mode.MULTIPLAYER || currentMode == Mode.GENERATION_MULTI){
							if(playerNum == board.getMe()){
								if (direction.equals(Direction.UP)) {
									board.addMoveMultiplayer("Me Up");
								} else if (direction.equals(Direction.DOWN)) {
									board.addMoveMultiplayer("Me Down");
								} else if (direction.equals(Direction.LEFT)) {
									board.addMoveMultiplayer("Me Left");
								} else if (direction.equals(Direction.RIGHT)) {
									board.addMoveMultiplayer("Me Right");
								}
							}
							if(playerNum == board.getOtherPlayer()){
								if (direction.equals(Direction.UP)) {
									board.addMoveMultiplayer("Other Up");
								} else if (direction.equals(Direction.DOWN)) {
									board.addMoveMultiplayer("Other Down");
								} else if (direction.equals(Direction.LEFT)) {
									board.addMoveMultiplayer("Other Left");
								} else if (direction.equals(Direction.RIGHT)) {
									board.addMoveMultiplayer("Other Right");
								}						
							}
						}
						
						board.incrementScore();					

						board.moveBox(playerNum, direction);
						if (board.playerHasLavaWrapper(playerNum, direction))  {
							board.restartLevel();
 						} else {
 							board.movePlayer(playerNum, direction);
 						}

						board.isCompleted();
						
						if (board.createLevelInit()){
							System.out.println("Hello");
							board.changeCreateLevelInit();
							if(board.createLevelInitLoad()){
								board.changeCreateLevelInitLoad();
							}
							else{
								board.clearLevels();
							}
							customBuilds();
						}
						
					}
				}
			}
			
		
			if (key == KeyEvent.VK_T){
				System.out.println(board.getMode());
				Mode currentM = board.getMode();
				if(currentM .equals(Mode.CUSTOM_MODE_MAKING)){
					System.out.println("Pressed T");
					//Check if the Number of Boxes and the Goals are the same number
					if(board.numberOfBoxes()!= board.numberOfGoals() ){
						System.out.println("Number of Boxes Does Not Equal Number of Goals");
						return;
					}
					//Only allow 1 player to play
					if(board.numberOfPlayers() != 1){
						return;
					}
					//Remove all the Buttons Change Modes
					drawBoard.removeAllButtons();
					board.clearItems();
					board.initWorld();
					//Copies level into initialLevel within Board
					board.setUpForTest();
					board.setMode(Mode.CUSTOM_MODE_TEST);
					
							
				}
				if (currentM .equals(Mode.CUSTOM_MODE_TEST)){
					System.out.println("T Custom Mode test");
					board.setMode(Mode.CUSTOM_MODE_MAKING);
					customBuilds();
				}
							
			}
			
			repaint();	// update the view
			
		}
	}
}
