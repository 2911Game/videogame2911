import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;

/**
 * This class represents a Goal square.
 * Constructor loads the image for this Goal.
 * A Goal cannot be moved.
 * 
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public class Goal extends Actor {

	/**
	 * Modified constructor for Goal to load the image for this instance.
	 * 
	 * @param x	x-coordinate of bottom-right corner of this tile
	 * @param y y-coordinate of bottom-right corner of this tile
	 */
	public Goal(int x, int y) {
        super(x, y);

        URL loc = this.getClass().getResource("icons/grass_patch.png");
        ImageIcon icon = new ImageIcon(loc);
        Image image = icon.getImage();
        setImage(image);
    }
	
}
