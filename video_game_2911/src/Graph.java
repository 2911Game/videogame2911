import java.util.ArrayList;
import java.util.HashMap;

/**
 * An undirected graph with adjacency HashMap representation.
 * For finding connected components.
 *
 * Modified from Emily Chen's (z5098910) campervan assignment (a different Graph class was submitted)
 */
public class Graph {
		
	// for connected components
	private int numComponents;					// number of connected components in the graph
	private int[][] component;		// component number each vertex is in
	private int numCounted;						// number of vertices counted for which component they are in
	
	// graph representation
	private Item[][] level;
	private int numRows;
	private int numCols;
	
	/**
	 * Argument constructor for Graph. Initializes graph to given start and end vertices and edgeWeights.
	 * 
	 * @pre given ArrayLists are all the same size
	 * @pre each index of each ArrayList corresponds to the same index of the other ArrayLists
	 * 		i.e. index 0 of start, end, edgeWeight correspond to the same edge
	 * @param start			ArrayList of starting vertices for an edge
	 * @param end			ArrayList of corresponding ending vertices for an edge
	 * @param edgeWeights	ArrayList of corresponding edge weights for an edge
	 */
	public Graph(Item[][] level, int numRows, int numCols) {
		
		this.level = level;
		this.numRows = numRows;
		this.numCols = numCols;
		
		// an edge is Item.PATH, Item.BOX, and Item.GOAL
		// -1 is Item.WALL (disconnected)
		
		numComponents = 0;
		component = new int[numRows][numCols];
		// initialize all vertices to the -1
		for (int row = 0; row < numRows; row++) {
			for (int col = 0; col < numCols; col++) {
				component[row][col] = -1;	// this is "invalid", i.e. not connected
			}
		}
		
		findComponents();		// initialize component HashMap to the component numbers each vertex is in
	}
	
	
	/**
	 * Test to see if this edge exists in the graph.
	 * 
	 * @param row	vertex this edge starts at
	 * @param col	vertex this edge ends at
	 * @return	true if this edge exists in the graph, otherwise false
	 */
	public boolean hasEdge(int row, int col) {
		
		if (level[row][col].equals(Item.PATH) || level[row][col].equals(Item.BOX) 
				|| level[row][col].equals(Item.GOAL)) {
			return true;
		} else {
			return false;
		}

	}
	
	/**
	 * Getter for the weight of an edge specified by its vertices.
	 * 
	 * @pre this edge exists in the graph, call hasEdge(start, end) before calling this
	 * 		(can't give an indicator value if using generics and don't know what W is)
	 * @param start	vertex this edge starts at
	 * @param end	vertex this edge ends at
	 * @return	edge weight if this edge exists in the graph
	 */
	/*
	public Integer getEdge(Item start, Item end) {
		HashMap<Item, Integer> tmp = g.get(start);
		return tmp.get(end);
	}
	*/
	
	/**
	 * Getter for all the neighbors (adjacent vertices) of the given start vertex.
	 * 
	 * @param start	start vertex to look for neighbors for
	 * @return array list of vertices that start is connected to if it exists in the graph, otherwise an empty ArrayList
	 */
	/*
	public ArrayList<Item> getNeighbors(Item start) {
		
		ArrayList<Item> neighbors = new ArrayList<Item>();
		
		if (g.containsKey(start)) {
			// vertex exists
			HashMap<Item, Integer> tmp = g.get(start);
			for (Item end: tmp.keySet()) {
				// add each end vertex to the array list
				neighbors.add(end);
			}
		}
		
		// will be empty if vertex doesn't exist
		return neighbors;
	}
	*/
	
	/**
	 * Getter for the size of this graph (i.e. number of vertices).
	 * 
	 * @return	number of vertices in this graph
	 */
	/*
	public int getSize() {
		return g.size();
	}
	*/
	
	/**
	 * Getter for the number of vertices adjacent to the parameter vertex (i.e. num of neighbors).
	 * 
	 * @param vertex	vertex to find neighbors for
	 * @return	number of vertices this vertex is connected to, if the vertex doesn't exist then return 0
	 */
	/*
	public int getNumNeighbors(Item vertex) {
	
		if (!(g.containsKey(vertex))) {
			// vertex doesn't exist in graph
			return 0;
		} else {
			return g.get(vertex).size(); // size of adjacency HashMap
		}
	}
	*/
	
	/**
	 * Prints a description of information in this graph to standard output.
	 * Useful for debugging.
	 * 
	 * @pre vertices of type V should have a valid toString function implemented for this to be useful
	 */
	public void printLevel() {
		for (int row = 0; row < Dimensions.NUM_ROWS; row++) {
			for (int col = 0; col < Dimensions.NUM_COLS; col++) {
				System.out.print(level[row][col] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	/**
	 * Runs the Floyd Warshall Algorithm to calculate the shortest path between all pairs of vertices in the graph.
	 * Note this can replace the components array for testing for connected components if you call it first.
	 * 
	 * @see http://www.programming-algorithms.net/article/45708/Floyd-Warshall-algorithm
	 * @see http://www.geeksforgeeks.org/dynamic-programming-set-16-floyd-warshall-algorithm/
	 * @pre the edge weights are integer types
	 * @return	solution matrix with the innermost value being the cost of the shortest path between the vertices,
	 * 			or empty solution matrix if W is not an instance of Integer
	 */
	/*
	public HashMap<Item, HashMap<Item, Integer>> floydWarshall() {
		HashMap<Item, HashMap<Item, Integer>> sol = new HashMap<Item, HashMap<Item, Integer>>();
		
		HashMap<Item, Integer> tmp;	// temporary inner hashmap for inserting values
		
		
		
		// initialize solution matrix
		// if the two vertices are connected, weight is the weight of the edge
		// else, it is "infinity"
		for (Item start: g.keySet()) {
			
			for (Item end: g.keySet()) {
				// are start and end connected?
				tmp = new HashMap<Item, Integer>();
				if (hasEdge(start, end)) {
					// has an edge, weight is weight of that edge
					tmp.put(end, getEdge(start, end));
					sol.put(start, tmp);
				} else {
					// no edge, weight is "infinity"
					//tmp.put(end, W.MAX_VALUE);
				}
			}
			
		}
		
		return sol;
	}
	*/
	
	/**
	 * Test to see if two vertices are in the same connected component.
	 * 
	 * @pre component array is initialized
	 * @pre called findComponents() before this
	 * @param start		first vertex to test if it's in the same connected component
	 * @param end		second vertex to test if it's in the same connected component
	 * @return	true if start and end are in the same connected component, false otherwise
	 */
	public boolean isConnected(Item start, Item end) {
		//System.out.println(start.toString() + " is in component " + component.get(start).toString());
		//System.out.println(end.toString() + " is in component " + component.get(end).toString());
		
		if (component.get(start) == component.get(end)) {
			// same connected component
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Calculates the number of connected components in the graph and assigns each vertex to a component.
	 * Uses the algorithm given in COMP1927 16s2 algorithms almanac.
	 * 
	 * @see 	"Connected Components" in https://www.cse.unsw.edu.au/~cs1927/16s2/exam/14s2/Algos/index.html
	 * @pre		component contains all the vertices in the graph and all map to -1
	 * @post 	numComponents gives the number of connected components in the graph
	 * @post 	each vertex V in the HashMap component maps to the connected component ID it is in (0..numComponents)
	 */
	private void findComponents() {
		numCounted = 0; // numCounted is incremented in dfsComponents as well
		
		while (numCounted < g.size()) {
			// find next vertex without a valid component number
			for (Item vertex: g.keySet()) {
				if (component.get(vertex) == -1) {
					//System.out.println("in the if!!");
					// this vertex is in a new connected component, 
					// go find all vertices in this new connected subgraph
					dfsComponents(vertex);
					numComponents += 1;		// finished labeling one connected component
					break;
				}
			}
			
		}
	}
	
	/**
	 * Recursive depth-first search helper method for findComponents.
	 * Marks all vertices in this connected component with the same component ID
	 * 
	 * @post all vertices in this component will be marked to the same integer in component
	 * @param vertex	one vertex in this connected component (to find all the other vertices from)
	 */
	private void dfsComponents(Item vertex) {
		component.replace(vertex, numComponents);	// numComponents is current component ID
		numCounted += 1;	// counted one more vertex
		// now find all other vertices of this connected subgraph
		ArrayList<Item> neighbors = getNeighbors(vertex);
		for (Item adj: neighbors) {
			// has it been set to a component yet?
			if (component.get(adj) == -1) {
				// hasn't been set to a component, use recursion
				dfsComponents(adj);
			}
		}
	}
}
