/**
 * The horizontal distance between two points
 * 
 * @author emilychen97
 *
 */
public class HorizontalNorm implements Norm {

	@Override
	public double calcNorm(Coordinate a, Coordinate b) {
		return Math.abs(a.getX() - b.getX());
	}

}
