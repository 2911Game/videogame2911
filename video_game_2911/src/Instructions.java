import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * This class acts as the view for the instructions.
 * It is to be displayed towards the righthand side of the screen.
 * 
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public class Instructions extends JPanel {
	
	private Board board;
	
	/**
	 * Constructor for Instructions.
	 * Takes in a reference to a Board to decide which instruction panel to show when.
	 * 
	 * @param board Board that represents the state of a Pikachu Push game.
	 */
	public Instructions(Board board) {
		Dimension dimensions = new Dimension(8 * Dimensions.SPACE, Dimensions.NUM_ROWS * Dimensions.SPACE);
		setPreferredSize(dimensions);
		setFocusable(false);
		this.board = board;
		setBackground(new Color(139, 219, 104));
		setLayout(new GridBagLayout());
		
	}
	
	/**
	 * Draws the ITEM's onto the rectangle and updates the view
	 */
	public void drawInstructions(Graphics g) {
		URL loc = null;
		Mode currentMode = board.getMode();
		
		if (currentMode == Mode.START_SCREEN || currentMode == Mode.LOAD_SCREEN) {
			loc = this.getClass().getResource("icons/instructionsStartScreen.png");
		} else if (currentMode == Mode.SINGLE_PLAYER ||
				   currentMode == Mode.EASY || currentMode == Mode.RANDOM_EASY ||
				   currentMode == Mode.MEDIUM || currentMode == Mode.RANDOM_MEDIUM ||
				   currentMode == Mode.HARD || currentMode == Mode.RANDOM_HARD ||
				   currentMode == Mode.CUSTOM_MODE_LOAD || currentMode == Mode.GENERATION) {
			loc = this.getClass().getResource("icons/instructionsSinglePlayer.png");
		} else if (currentMode == Mode.MULTIPLAYER || currentMode == Mode.GENERATION_MULTI) {
			loc = this.getClass().getResource("icons/instructionsMultiPlayer.png");
		} else if (currentMode == Mode.CUSTOM_MODE_TEST && !board.isDone()) {
			loc = this.getClass().getResource("icons/instructionsCustomModeTest.png");
		} else if (currentMode == Mode.CUSTOM_MODE_TEST && board.isDone()) {
			loc = this.getClass().getResource("icons/instructionsCustomModeTestDone.png");
		} else if (currentMode == Mode.CUSTOM_MODE_MAKING) {
			loc = this.getClass().getResource("icons/instructionsCustomModeMaking.png");
		}
		
        Image image = (new ImageIcon(loc)).getImage();
        g.drawImage(image, 0, 0, this);
        
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("fonts/Pokemon_GB.ttf");
		Font font = null;
		try {
			font = Font.createFont(Font.TRUETYPE_FONT, inputStream);
			Font sizedFont = font.deriveFont(Font.BOLD, 16f);
			g.setFont(sizedFont);
		} catch (FontFormatException | IOException e1) {
			// do nothing and default to default font
		}
    
        if (!(board.getLevelNum() == 0) && !board.isDone()) {	
        	g.drawString("Level : " + board.getLevelNum(), Dimensions.SPACE, Dimensions.SPACE + 20);
        	g.drawString("Moves : " + board.getScore(), Dimensions.SPACE, Dimensions.SPACE);
        }
	}
	
	/**
	 * Overrides JComponent's paint method to draw all the Item's as well
	 * 
	 * @see JComponent
	 * @param g	Graphics object to be painted
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		drawInstructions(g);
	}
}
