/**
 * Enum type for all the Item's that can show up on the map.
 * Used to render a 2D matrix with the initial map state to 
 * something the Board can render into graphics.
 * 
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public enum Item {
	PLAYER, WALL, BOX, GOAL, EMPTY, LAVA
}
