import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;

/**
 * Represents "acid" in the game (was called Lava during creation) then refactored to fit theme.
 * Lava cannot move. 
 *
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public class Lava extends Actor {
    
	/**
	 * Constructor for Lava to specify its x, y coordinates (in terms of pixels)
	 * 
	 * @param x	x-coordinate of bottom-right corner of this tile
	 * @param y y-coordinate of bottom-right corner of this tile
	 */
	public Lava(int x, int y) {
		super(x, y);

		URL loc = this.getClass().getResource("icons/lava.png");	
		ImageIcon icon = new ImageIcon(loc);	
		Image img = icon.getImage();	
		setImage(img);	
	}

}
