import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * This class generates levels.
 * Can specify the type of level you want with LvlCode's in the getLevel function.
 * Can generate hard-coded blank levels and a test level, and randomly generates
 * easy, medium, and hard maps for single and multiplayer modes.
 *
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public class LevelGenerator {
	private Item[][] level;		// each level is represented by a 2D array of a fixed size (constants in Dimensions)
	
	private static final int PADDING = 2;	// amount to pad moves so you can insert an outer wall and let the player move
	
	private static final int MIN_MOVES_EASY = 3000;		// range to generate a maximum number of moves from
	private static final int MAX_MOVES_EASY = 3010;
	private static final int MIN_MOVES_MEDIUM = 3000;
	private static final int MAX_MOVES_MEDIUM = 3010;
	private static final int MIN_MOVES_HARD = 5000;
	private static final int MAX_MOVES_HARD = 5010;
	
	// density of walls in easy, medium, hard levels (excluding outer walls)
	private static final double EASY_DENSITY = 4.0;
	private static final double MEDIUM_DENSITY = 8.0;
	private static final double HARD_DENSITY = 8.0;
	
	// density of lava in relation to WALLs in easy, medium, hard levels
	private static final double EASY_LAVA_DENSITY = 20.0;
	private static final double MEDIUM_LAVA_DENSITY = 30.0;
	private static final double HARD_LAVA_DENSITY = 50.0;
	
	// subset of map to generate on (large maps cause Player to not touch boxes in most iterations, even with many moves)
	private static final int MIN_ROW = 4;	
	private static final int MAX_ROW = 13;
	private static final int MIN_COL = 4;
	private static final int MAX_COL = 16;
	
	// constructor
	/**
	 * Initializes the level field to a blank level.
	 */
	public LevelGenerator() {
		level = initBlankLevel();	// call this no matter what level you generate
	}
	
	/**
	 * Returns a level (randomly generated or hardcoded).
	 * 
	 * @param code			LvlCode specifying type of level you want returned	
	 * @return				Item[][] matrix of the dimensions in Dimensions initialized to the desired level
	 */
	public Item[][] getLevel(LvlCode code) {
		if (code.equals(LvlCode.TEST)) {
			initTestLevel();
		} else if (code.equals(LvlCode.TEST_MULTI)) {
			initTestMultiLevel();
		} else if (code.equals(LvlCode.BLANK)) {
			level = initBlankLevel(); 	// just in case
		} else {
			// random levels
			do {
				System.out.println("Thinking of a level...");
				initRandomMove(code);
			} while(boxGoalMatched() == false || numPlayersMatched(code) == false);	
			// regenerate if there aren't the same number of goals as boxes, 
			// or if you don't have the number of players that you wanted on the map
		}
		
		return level;
	}
	
	/**
	 * Checks to see that the number of players in level is the same as needed in the mode.
	 * 
	 * @param mode	LvlCode for the type of level to generate.
	 * @return	for single player modes, true if 1 player is seen, false otherwise
	 * 			for multiplayer modes, true if 2 players are seen, false otherwise
	 */
	private boolean numPlayersMatched(LvlCode mode) {
		int numSeen = 0;
		
		// count the number of players you see in level
		for (int row = 0; row < Dimensions.NUM_ROWS; row++) {
			for (int col = 0; col < Dimensions.NUM_COLS; col++) {
				if (level[row][col] == Item.PLAYER) {
					numSeen += 1;
				}
			}
		}
		
		// check if this is the right number
		if (mode == LvlCode.EASY || mode == LvlCode.MEDIUM || mode == LvlCode.HARD) {
			if (numSeen == 1) {
				return true;
			} else {
				return false;
			}
		} else {
			// multiplayer mode
			if (numSeen == 2) {
				return true;
			} else {
				return false;
			}
		}
		
	}
	
	/**
	 * Looks at a level and sees if there are the same number of Item.GOAL as Item.BOX
	 * 
	 * @return	true if there are the same number of GOAL's as BOXes, false otherwise
	 */
	private boolean boxGoalMatched() {
		int numGoals = 0;
		int numBoxes = 0;
		
		// count the number of GOAL's and BOXes
		for (int row = 0; row < Dimensions.NUM_ROWS; row++) {
			for (int col = 0; col < Dimensions.NUM_COLS; col++) {
				if (level[row][col] == Item.GOAL) {
					numGoals += 1;
				} else if (level[row][col] == Item.BOX) {
					numBoxes += 1;
				}
			}
		}
		
		if (numGoals == numBoxes) {
			return true;
		} else {
			return false;
		}
		
	}
	

	/**
	 * Initializes an empty level.
	 * Call this before hardcoding solutions / placing objects in places / telling algos to put items on places.
	 * 
	 * @return	Item[Dimensions.NUM_COLS][Dimensions.NUM_COLS] with every item initialized to Item.EMPTY
	 */
	private Item[][] initBlankLevel() {
		Item[][] blank = new Item[Dimensions.NUM_ROWS][Dimensions.NUM_COLS];
		int row, col;	// counters
		
		// initialize everything to empty
		for (row = 0; row < Dimensions.NUM_ROWS; row++) {
			for (col = 0; col < Dimensions.NUM_COLS; col++) {
				blank[row][col] = Item.EMPTY;
			}
		}
		
		return blank;
	}
	
	// ======== Randomly-Generated Levels =====
	
	/**
	 * Generates random levels of different LvlCode types by moving a player around a map.
	 * First determines the traits that this level must have given the LvlCode.
	 * Then inserts a border for the subset of the map we are working with and inserts random walls.
	 * The density of walls inserted relative to empty space is determined by the class constants.
	 * Next a player and the required number of boxes are randomly placed on empty spaces on the level.
	 * An instance of DummyLevel is used to physically move the player around this map and push the boxes around,
	 * thus guaranteeing the level is solvable. The final coordinates of the boxes after max moves is returned,
	 * and Item.GOAL's are placed there. Finally, if required, a second player is placed on an empty space on the map.
	 * 
	 * @param mode type of level to randomly generate as specified by LvlCode
	 */
	private void initRandomMove(LvlCode mode) {
		level = initBlankLevel();
		
		ArrayList<Coordinate> startList = new ArrayList<Coordinate>();	// coordinates of all start boxes
		ArrayList<Coordinate> goals;									// coordinates of goals
		int maxMoves;
		ArrayList<Coordinate> playerCoords = new ArrayList<Coordinate>();
		ArrayList<Box> boxes = new ArrayList<Box>();
		
		int numBoxes = 0;
		
		// determine the traits of this level
		if (mode.equals(LvlCode.EASY)) {
			numBoxes = 2;
			maxMoves = randomInt(MIN_MOVES_EASY, MAX_MOVES_EASY);
		} else if (mode.equals(LvlCode.MEDIUM)) {
			numBoxes = 3;
			maxMoves = randomInt(MIN_MOVES_MEDIUM, MAX_MOVES_MEDIUM);
		} else if(mode.equals(LvlCode.HARD)) {
			numBoxes = 6;	
			maxMoves = randomInt(MIN_MOVES_HARD, MAX_MOVES_HARD);
		} else if (mode.equals(LvlCode.EASY_MULTI)) {
			numBoxes = 2;
			maxMoves = randomInt(MIN_MOVES_EASY, MAX_MOVES_EASY);
		} else if (mode.equals(LvlCode.MEDIUM_MULTI)) {
			numBoxes = 3;
			maxMoves = randomInt(MIN_MOVES_MEDIUM, MAX_MOVES_MEDIUM);
		} else {
			numBoxes = 6;
			maxMoves = randomInt(MIN_MOVES_HARD, MAX_MOVES_HARD);
		}
		
		// insert walls first
		insertBorder();
		insertWalls(mode);
		
		int x, y;
		// place the player that will move
		x = randomInt(MIN_COL + 1, MAX_COL - 1);
		y = randomInt(MIN_ROW + 1, MAX_ROW - 1);
		level[y][x] = Item.PLAYER;
		playerCoords.add(new Coordinate(x, y));
		
		// place boxes on the level
		for (int i = 0 ; i < numBoxes; i++) {
			int xStart, yStart;
			Coordinate start;
			do {
				xStart = randomInt(MIN_COL + 1, MAX_COL - 1);	// leave 2 space padding on each side for walls + Player
				yStart = randomInt(MIN_ROW + 1, MAX_COL - 1);
				start = new Coordinate(xStart, yStart);
			} while (coordinateInList(start, startList) == true ||
					coordinateInList(start, playerCoords) == true ||
					onWall(start) == true);	// generate a new coordinate if same start, start on a player, or start on a wall
			
			// add this coordinate as a starting coordinate, and make a Box
			startList.add(start);
			boxes.add(new Box(xStart, yStart));
			
			// place Box at the start
			level[start.getY()][start.getX()] = Item.BOX;
		}
				
		
		int historySize = 4;	// size of the moveHistory
		Coordinate[] history = new Coordinate[historySize];	// keep track of player's past moves 
		// initialize pastMoves all to the starting coordinate of the player
		for (int i = 0; i < historySize; i++) {
			history[i] = playerCoords.get(0);
		}
		
		
		// generate a dummy board to simulate movements
		DummyBoard test = new DummyBoard(level);
		
		// move each player maxMoves number of times
		for (int i = 0; i < maxMoves; i++) {
			
			Direction dir;
			// correct to move away from the last location in history every 3 rounds
			// prevents the player from circling around one local area for too long
			if (i % 3 == 0) {
				dir = farDirection(history, historySize);
			} else {
				dir = randomDirection();
			}
			
			test.movePlayer(dir);
			
			history[i % historySize] = test.getPlayerPos();
		}
		
		// get the coordinates of the final positions of the boxes, these will be the goals
		goals = test.getBoxCoordinates();
		
		// place goals on the map
		for (Coordinate goal: goals) {
			int xCoord = goal.getX();
			int yCoord = goal.getY();
			level[yCoord][xCoord] = Item.GOAL;
		}
		
		// change some walls to lava
		insertLava(mode);
		
		if (mode == LvlCode.EASY_MULTI || mode == LvlCode.MEDIUM_MULTI || mode == LvlCode.HARD_MULTI) {
			// place a second player for multiplayer mode
			placePlayer();
		}
		
	}
	
	
	/**
	 * Generates a pseudo-random direction that moves the Player away from its last item in history.
	 * Generates coordinates for all possible directions, then calculates the Euclidean distance
	 * to the last item in history. Randomly picks a direction from the furthest two distances.
	 * 
	 * @param history	the last "size" Coordinates that the Player has been to
	 * @param size		the size of the history array
	 * @return a Direction that is one of the top two furthest distances from the last move in history.
	 */
	private Direction farDirection(Coordinate[] history, int size) {
		Coordinate up, down, left, right;
		Coordinate prev;
		double[] distances = new double[4];			// index 0 = up, index 1 = down, index 2 = left, index 3 = right
		Coordinate[] possible = new Coordinate[4];	// 4 possible coordinates.
		Direction[] directions = new Direction[4];	// 4 possible directions
		int furthest, secondFurthest;	// index of array of possible of the furthest and second furthest Coordinates from history[size - 1]
		
		// generate a coordinate for each of the possible directions to move in
		prev = history[0];
		up = new Coordinate(prev.getX(), prev.getY() - 1);
		down = new Coordinate(prev.getX(), prev.getY() + 1);
		left = new Coordinate(prev.getX() - 1, prev.getY());
		right = new Coordinate(prev.getX() + 1, prev.getY());
		possible[0] = up;
		directions[0] = Direction.UP;
		possible[1] = down;
		directions[1] = Direction.DOWN;
		possible[2] = left;
		directions[2] = Direction.LEFT;
		possible[3] = right;
		directions[3] = Direction.RIGHT;
		
		// calculate the Euclidean distance of each Coordinate from the last item in history
		Norm euclid = new EuclideanNorm();
		for (int i = 0; i < 4; i++) {
			distances[i] = euclid.calcNorm(possible[i], history[size - 1]);
		}
		
		// find the furthest coordinate
		furthest = 0;
		for (int i = 1; i < 4; i++) {
			if (distances[i] > distances[furthest]) {
				// found a new furthest index
				furthest = i;
			}
		}
		
		// find the second furthest coordinate
		if (furthest != 0) {
			secondFurthest = 0;
		} else {
			secondFurthest = 1;
		}
		for (int i = 0; i < 4; i++) {
			if (i == furthest) {
				// can't be furthest and second furthest
				continue;
			}
			
			if (distances[i] > distances[secondFurthest]) {
				secondFurthest = i;
			}
		}
		
		
		Random rand = new Random();
		int chance = rand.nextInt(50) % 2;
		if (chance == 0) {
			return directions[furthest];
		} else {
			return directions[secondFurthest];
		}
	}
	
	/**
	 * Tests if the given Coordinate is that of an Item.WALL in level.
	 * 
	 * @param test	coordinate to test
	 * @return	true if test coincides with a WALL, false otherwise
	 */
	private boolean onWall(Coordinate test) {
		if (level[test.getY()][test.getX()] == Item.WALL) {
			return true;
		} else {
			return false;
		}
	}
		
	/**
	 * Checks if a given coordinate is in an ArrayList.
	 * If we overwrote equals and haschode for Coordinate would not need this function.
	 * But overwriting equals can be finicky.
	 * 
	 * @param point		coordinate to check if it is in list
	 * @param list	list of Coordiantes to check if x is in
	 * @return	true if x is in list, else false
	 */
	private boolean coordinateInList (Coordinate point, ArrayList<Coordinate> list) {
		for (Coordinate candidate: list) {
			if ((point.getX() == candidate.getX()) && (point.getY() == candidate.getY())) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Places a player somewhere on an EMPTY spot.
	 */
	private void placePlayer() {
		ArrayList<Coordinate> possible = new ArrayList<Coordinate>();
		Coordinate tmp, chosen;
		
		// go through level and generate a Coordinate for every PATH, add to ArrayList
		for (int row = MIN_ROW + 1; row <= MAX_ROW - 1; row++) {
			for (int col = MIN_COL; col <= MAX_COL - 1; col++) {
				
				if (level[row][col].equals(Item.EMPTY)) {
					// generate a coordinate for this EMPTY and add to arraylist
					tmp = new Coordinate(col, row);
					possible.add(tmp);
				}
				
			}
		}
		
		// generate a random index for possible
		int index = randomInt(0, possible.size() - 1);
		chosen = possible.get(index);
		
		// place player at the chosen coordinates
		level[chosen.getY()][chosen.getX()] = Item.PLAYER;
	}
	
	
	/**
	 * Changes some of the inner walls of the map to Lava.
	 *
	 * @pre all walls have been initialized on the map, no calls to DummyBoard will be made
	 * 		(calls to DummyBoard with lava on the board result in unsolvable puzzles)
	 * @param difficulty difficulty mode for this map
	 */
	private void insertLava(LvlCode mode) {
		ArrayList<Coordinate> walls = new ArrayList<Coordinate>();
		double target;
		
		if (mode == LvlCode.EASY || mode == LvlCode.EASY_MULTI) {
			target = EASY_LAVA_DENSITY;
		} else if (mode == LvlCode.MEDIUM || mode == LvlCode.MEDIUM_MULTI) {
			target = MEDIUM_LAVA_DENSITY;
		} else {
			// (mode == LvlCode.HARD || mode == LvlCode.HARD_MULTI)
			target = HARD_LAVA_DENSITY;
		}
		
		// go through the level and get the coordinates of all the non-border walls
		for (int row = MIN_ROW + 1; row <= MAX_ROW - 1; row++) {

			for (int col = MIN_COL + 1; col <= MAX_COL - 1; col++) {
				if (level[row][col] == Item.WALL) {
					walls.add(new Coordinate(col, row));
				}
				
			}
		}
		
		// generate some random indices for walls based on the density
		double numToConvert = walls.size() * (target / 100);	// need to convert these many WALL's to LAVA
		System.out.println("walls.size() = " + walls.size() + ", numToConvert = " + numToConvert);
		int numConverted = 0;	// number of WALL's we have already converted to LAVA
		Random rand = new Random();
		int chance;
		int x, y;			// components of coordinates we have chosen
		Coordinate chosen;	// Coordinate we have chosen
		do {
			chance = rand.nextInt(walls.size() - 1);	// get a random index for walls
			// convert WALL to a LAVA
			chosen = walls.get(chance);
			x = chosen.getX();
			y = chosen.getY();
			level[y][x] = Item.LAVA;
			numConverted += 1;
		} while (numConverted < numToConvert);
		
	}
	
	/**
	 * Inserts walls into the map with density based on difficulty.
	 * Walls are inserted as single walls randomly. No patterns are used.
	 * 
	 * @param difficulty	difficulty of this level mode, determines density of boxes
	 */
	private void insertWalls(LvlCode difficulty) {
		// density of walls that we want to achieve
		double targetDensity;
		
		if (difficulty.equals(LvlCode.EASY)) {
			targetDensity = EASY_DENSITY;
		} else if (difficulty.equals(LvlCode.MEDIUM)) {
			targetDensity = MEDIUM_DENSITY;
		} else {
			targetDensity = HARD_DENSITY;
		}
		
		// put in edge walls to not confuse the user
		insertBorder();
		
		int initialEmpty = countEmpty();	// number of Item.EMPTY initially (after border)
		Coordinate tmp;	// temporary coordinate for a new wall
		int increment = 2;	// number of walls to place before checking density
		
		do {
			// try to insert walls increment at a time, then check if density is pass the threshold
			for (int count = 0; count < increment; count++) {
				tmp = randomCoordinateRange(MIN_COL, MAX_COL, MIN_ROW, MAX_ROW);
				if (level[tmp.getY()][tmp.getX()].equals(Item.EMPTY)) {
					// place a wall here
					level[tmp.getY()][tmp.getX()] = Item.WALL;
				}
				
			}
		} while(calcWallDensity(initialEmpty) < targetDensity);
		
	}
	
	/**
	 * Generates a random coordinate for the level within the specified range.
	 * 
	 * @pre min is not equal to max for x and y coordinates
	 * @param xMin	minimum x-value for the coordinate
	 * @param xMax	maximum x-value for the coordinate
	 * @param yMin	minimum y-value for the coordinate
	 * @param yMax	maximum y-value for the coordinate
	 * @return	a random Coordinate in the specified x and y ranges
	 */
	private Coordinate randomCoordinateRange(int xMin, int xMax, int yMin, int yMax) {
		int x, y;
		x = randomInt(xMin, xMax);
		y = randomInt(yMin, yMax);
		
		return new Coordinate(x, y);
	}
	
	/**
	 * Counts the number of tiles in level that are EMPTY.
	 * 
	 * @return count of number of EMPTY elements in level
	 */
	private int countEmpty() {
		int count = 0;
		
		for (int row = 0; row < Dimensions.NUM_ROWS; row++) {
			for (int col = 0; col < Dimensions.NUM_COLS; col++) {
				
				// if Item is empty, increment count
				if (level[row][col].equals(Item.EMPTY)) {
					count += 1;
				}
			}
		}
		
		return count;
	}
	
	/** 
	 * Returns the % of the Map that is composed of Walls, excluding edge walls.
	 * 
	 * @param initial number of EMPTY tiles (once all non-wall features have been added and border has been added) 
	 * @return density of currently empty tiles relative to initial empty
	 */
	private double calcWallDensity(int initialEmpty) {
		double emptyDensity;	//calculates the % empty relative to initialEmpty
		int numEmpty = countEmpty();
		
		emptyDensity = (double)numEmpty / initialEmpty * 100;
		
		return 100 - emptyDensity;
	}
	
	/**
	 * Puts border walls into the map
	 */
	private void insertBorder() {
		// put in horizontal strips
		for (int col = MIN_COL; col <= MAX_COL; col++) {
			// top 
			level[MIN_ROW][col] = Item.WALL;
			// bottom
			level[MAX_ROW][col] = Item.WALL;
		}
			
		// put in vertical strips
		for (int row = MIN_ROW; row <=  MAX_ROW; row++) {
			// left
			level[row][MIN_COL] = Item.WALL;
			// right
			level[row][MAX_COL] = Item.WALL;
		}
	}
	
	/**
	 * Generates a random Direction.
	 * 
	 * @return	randomly generated Direction
	 */
	private Direction randomDirection() {
		Random rand = new Random();
		int i = rand.nextInt(50) % 4;
		
		if (i == 0) {
			return Direction.UP;
		} else if (i == 1) {
			return Direction.DOWN;
		} else if (i == 2) {
			return Direction.LEFT;
		} else {
			return Direction.RIGHT;
		}
	}
	
	/**
	 * Generates a random integer in the given range.
	 * 
	 * @pre min is not equal to max, max is greater than min
	 * @param min	minimum value for this range
	 * @param max	maximum value for this range
	 * @return	randomly generated integer in the given range
	 */
	private int randomInt(int min, int max) {
		Random rand = new Random();
		int num = max - min;
		int range = Math.abs(num);
		
		
		int randomNum = rand.nextInt(range) + min; 
		
		return randomNum;
		
	}
	
	// ========= Hard-coded Levels ========
	
	/**
	 * Initializes a test level just do we can put something on the screen.
	 * Hard-coded level. Corresponds to LvlCode.TEST.
	 * 
	 * @pre level initialized to all Item.EMPTY first (call initBlankLevel())
	 */
	private void initTestLevel() {
		// hardcode map
		level[0][4] = Item.WALL;
		level[0][5] = Item.WALL;
		level[0][6] = Item.WALL;
		level[0][7] = Item.WALL;
		level[0][8] = Item.WALL;
		level[0][9] = Item.WALL;
		
		level[1][4] = Item.WALL;
		level[1][5] = Item.WALL;
		level[1][9] = Item.WALL;
		
		level[2][4] = Item.WALL;
		level[2][5] = Item.WALL;
		level[2][6] = Item.BOX;
		level[2][9] = Item.WALL;
		
		level[3][2] = Item.WALL;
		level[3][3] = Item.WALL;
		level[3][4] = Item.WALL;
		level[3][5] = Item.WALL;
		level[3][8] = Item.BOX;
		level[3][10] = Item.WALL;
		
		level[4][2] = Item.WALL;
		level[4][3] = Item.WALL;
		level[4][6] = Item.BOX;
		level[4][8] = Item.BOX;
		level[4][10] = Item.WALL;
		
		level[5][0] = Item.WALL;
		level[5][1] = Item.WALL;
		level[5][2] = Item.WALL;
		level[5][3] = Item.WALL;
		level[5][5] = Item.WALL;
		level[5][7] = Item.WALL;
		level[5][8] = Item.WALL;
		level[5][10] = Item.WALL;
		level[5][13] = Item.WALL;
		level[5][14] = Item.WALL;
		level[5][15] = Item.WALL;
		level[5][16] = Item.WALL;
		level[5][17] = Item.WALL;
		level[5][18] = Item.WALL;
		
		level[6][0] = Item.WALL;
		level[6][1] = Item.WALL;
		level[6][5] = Item.WALL;
		level[6][7] = Item.WALL;
		level[6][8] = Item.WALL;
		level[6][10] = Item.WALL;
		level[6][11] = Item.WALL;
		level[6][12] = Item.WALL;
		level[6][13] = Item.WALL;
		level[6][16] = Item.GOAL;
		level[6][17] = Item.GOAL;
		level[6][18] = Item.WALL;
		
		level[7][0] = Item.WALL;
		level[7][1] = Item.WALL;
		level[7][3] = Item.BOX; //BOX PREVIOUSLY
		level[7][6] = Item.BOX;
		level[7][16] = Item.GOAL;
		level[7][17] = Item.GOAL;
		level[7][18] = Item.WALL;
		
		level[8][0] = Item.WALL;
		level[8][1] = Item.WALL;
		level[8][2] = Item.WALL;
		level[8][3] = Item.WALL;
		level[8][4] = Item.WALL;
		level[8][5] = Item.WALL;
		level[8][7] = Item.WALL;
		level[8][8] = Item.WALL;
		level[8][10] = Item.WALL;
		level[8][11] = Item.PLAYER;
		level[8][12] = Item.WALL;
		level[8][13] = Item.WALL;
		level[8][16] = Item.GOAL;
		level[8][17] = Item.GOAL;
		level[8][18] = Item.WALL;
		
		level[9][4] = Item.WALL;
		level[9][5] = Item.WALL;
		level[9][10] = Item.WALL;
		level[9][11] = Item.WALL;
		level[9][12] = Item.WALL;
		level[9][13] = Item.WALL;
		level[9][14] = Item.WALL;
		level[9][15] = Item.WALL;
		level[9][16] = Item.WALL;
		level[9][17] = Item.WALL;
		level[9][18] = Item.WALL;
		
		level[10][4] = Item.WALL;
		level[10][5] = Item.WALL;
		level[10][6] = Item.WALL;
		level[10][7] = Item.WALL;
		level[10][8] = Item.WALL;
		level[10][9] = Item.WALL;
		level[10][10] = Item.WALL;
	}
	
	/**
	 * Initializes a test level just do we can put something on the screen.
	 * Hard-coded level. Corresponds to LvlCode.TEST_MULTI.
	 * 
	 * @pre level initialized to all Item.EMPTY first (call initBlankLevel())
	 */
	private void initTestMultiLevel() {
		// hardcode map
		level[8][14] = Item.PLAYER;		// this is player 2
		level[0][4] = Item.WALL;
		level[0][5] = Item.WALL;
		level[0][6] = Item.WALL;
		level[0][7] = Item.WALL;
		level[0][8] = Item.WALL;
		level[0][9] = Item.WALL;
		
		level[1][4] = Item.WALL;
		level[1][5] = Item.WALL;
		level[1][9] = Item.WALL;
		
		level[2][4] = Item.WALL;
		level[2][5] = Item.WALL;
		level[2][6] = Item.BOX;
		level[2][9] = Item.WALL;
		
		level[3][2] = Item.WALL;
		level[3][3] = Item.WALL;
		level[3][4] = Item.WALL;
		level[3][5] = Item.WALL;
		level[3][8] = Item.BOX;
		level[3][10] = Item.WALL;
		
		level[4][2] = Item.WALL;
		level[4][3] = Item.WALL;
		level[4][6] = Item.BOX;
		level[4][8] = Item.BOX;
		level[4][10] = Item.WALL;
		
		level[5][0] = Item.WALL;
		level[5][1] = Item.WALL;
		level[5][2] = Item.WALL;
		level[5][3] = Item.WALL;
		level[5][5] = Item.WALL;
		level[5][7] = Item.WALL;
		level[5][8] = Item.WALL;
		level[5][10] = Item.WALL;
		level[5][13] = Item.WALL;
		level[5][14] = Item.WALL;
		level[5][15] = Item.WALL;
		level[5][16] = Item.WALL;
		level[5][17] = Item.WALL;
		level[5][18] = Item.WALL;
		
		level[6][0] = Item.WALL;
		level[6][1] = Item.WALL;
		level[6][5] = Item.WALL;
		level[6][7] = Item.WALL;
		level[6][8] = Item.WALL;
		level[6][10] = Item.WALL;
		level[6][11] = Item.WALL;
		level[6][12] = Item.WALL;
		level[6][13] = Item.WALL;
		level[6][16] = Item.GOAL;
		level[6][17] = Item.GOAL;
		level[6][18] = Item.WALL;
		
		level[7][0] = Item.WALL;
		level[7][1] = Item.WALL;
		level[7][3] = Item.BOX; //BOX PREVIOUSLY
		level[7][6] = Item.BOX;
		level[7][16] = Item.GOAL;
		level[7][17] = Item.GOAL;
		level[7][18] = Item.WALL;
		
		level[8][0] = Item.WALL;
		level[8][1] = Item.WALL;
		level[8][2] = Item.WALL;
		level[8][3] = Item.WALL;
		level[8][4] = Item.WALL;
		level[8][5] = Item.WALL;
		level[8][7] = Item.WALL;
		level[8][8] = Item.WALL;
		level[8][10] = Item.WALL;
		level[8][11] = Item.PLAYER;
		level[8][12] = Item.WALL;
		level[8][13] = Item.WALL;
		level[8][16] = Item.GOAL;
		level[8][17] = Item.GOAL;
		level[8][18] = Item.WALL;
		
		level[9][4] = Item.WALL;
		level[9][5] = Item.WALL;
		level[9][10] = Item.WALL;
		level[9][11] = Item.WALL;
		level[9][12] = Item.WALL;
		level[9][13] = Item.WALL;
		level[9][14] = Item.WALL;
		level[9][15] = Item.WALL;
		level[9][16] = Item.WALL;
		level[9][17] = Item.WALL;
		level[9][18] = Item.WALL;
		
		level[10][4] = Item.WALL;
		level[10][5] = Item.WALL;
		level[10][6] = Item.WALL;
		level[10][7] = Item.WALL;
		level[10][8] = Item.WALL;
		level[10][9] = Item.WALL;
		level[10][10] = Item.WALL;
	}
}
