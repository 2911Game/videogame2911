import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

// have to turn functions into PUBLIC to test
public class LevelGeneratorTest {

	/*
	@Test
	public void testMakePath() {
		LevelGenerator gen = new LevelGenerator();
		
		Coordinate start = new Coordinate(8, 9);
		int maxMoves = 100;
		ArrayList<Coordinate> path;
		
		// test 1
		System.out.println("Test 1: start = " + start + ", maxMoves = " + maxMoves);
		path = gen.makePath(start, maxMoves);
		System.out.println("== Printing out Path ==");
		System.out.println("size = " + path.size());
		for (Coordinate c: path) {
			System.out.println(c.toString());
		}

	}
	*/
	
	/*
	@Test
	public void testCalcWallDensity() {
		LevelGenerator gen = new LevelGenerator();
		int initEmpty = 297;	// minus border wall, 1 player, 1 box, 1 goal
		
		
	}
	*/
	
	@Test
	public void testEuclidNorm() {
		Norm norm = new EuclideanNorm();
		
		Coordinate a = new Coordinate(2, 3);
		Coordinate b = new Coordinate(4, 5);
		System.out.println("Euclidean distance between " + a + " and " + b + "= " + norm.calcNorm(a, b));
	}
	
	@Test
	public void testGetLevel() {
		//System.out.println("HI I AM TEST OUTPUT!");
		LevelGenerator gen = new LevelGenerator();
		Item[][] level = gen.getLevel(LvlCode.HARD);
		System.out.println("======= Printing out generated level =======");
		for (int row = 0; row < Dimensions.NUM_ROWS; row++) {
			for (int col = 0; col < Dimensions.NUM_COLS; col++) {
				System.out.print(level[row][col] + " ");
			}
			System.out.println();
		}
		System.out.println();
		
		//System.out.println("Why didn't I print out anyting?");
	}
}
