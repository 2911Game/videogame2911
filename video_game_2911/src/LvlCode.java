/**
 * Enum types for the types of levels to generate / hard-coded levels to access
 * 
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public enum LvlCode {
	// hard-coded levels
	TEST, BLANK, TEST_MULTI,
	// randomly generated level modes
	EASY, MEDIUM, HARD,
	// randomly generated multiplayer levels
	EASY_MULTI, MEDIUM_MULTI, HARD_MULTI,
}
