/**
 * Enum type for different playing modes
 * Random = randomly generated levels (easy, medium, hard)
 * Easy, Medium, Hard = hardcoded storyline levels
 * Multiplayer = hardcoded storyline levels
 * 
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public enum Mode {
	SPLASH_SCREEN, START_SCREEN, LOAD_SCREEN, EASY, MEDIUM, HARD, MULTIPLAYER, SINGLE_PLAYER,
	RANDOM_EASY, RANDOM_MEDIUM, RANDOM_HARD, CUSTOM_MODE_TEST, CUSTOM_MODE_MAKING, CUSTOM_MODE_LOAD , 
	GENERATION, GENERATION_MULTI,
}
