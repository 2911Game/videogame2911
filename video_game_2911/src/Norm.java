/**
 * Strategy pattern interface for switching out norms.
 *
 */
public interface Norm {

	/**
	 * Calculate the norm between two points.
	 * Implement using a concrete norm type.
	 * 
	 * @param a		first point
	 * @param b 	second point
	 * @return	norm between the two points (2-D norm)
	 */
	public double calcNorm(Coordinate a, Coordinate b);
}
