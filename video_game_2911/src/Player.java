import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;

/**
 * This class represents a Player.
 * Constructor modified so it can load an image,
 * and Players can move.
 *
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public class Player extends Actor {

	/**
	 * Modified constructor for Player to load the image for this instance.
	 * 
	 * @param x	x-coordinate of bottom-right corner of this tile
	 * @param y y-coordinate of bottom-right corner of this tile
	 */
	public Player(int x, int y, int playerID) {
        super(x, y);
        URL loc = null ; // Just so an error isn't thrown
        if (playerID == 0) {
        	loc = this.getClass().getResource("icons/ash.png");
        } else {
        	loc = this.getClass().getResource("icons/gary.png");
        }
        ImageIcon icon = new ImageIcon(loc);
        Image image = icon.getImage();
        setImage(image);
    }

	/**
	 * Moves the Player by the given parameters.
	 * 
	 * @param direction in which to move the Player (by one space)
	 */
    public void move(Direction direction) {
    	int newX = getX();
        int newY = getY();
    	
    	if (direction.equals(Direction.UP)) {
            newY -= Dimensions.SPACE;
        } else if (direction.equals(Direction.DOWN)) {
            newY += Dimensions.SPACE;
        } else if (direction.equals(Direction.LEFT)) {
    		newX -= Dimensions.SPACE;
        } else if (direction.equals(Direction.RIGHT)) {
        	newX += Dimensions.SPACE;
        }
    	
    	if (!(newX < 0 || newX > Dimensions.SPACE * (Dimensions.NUM_COLS - 1)||
    		newY < 0 || newY > Dimensions.SPACE * (Dimensions.NUM_ROWS - 1))) {
    		setX(newX);
            setY(newY);
    	}
    }
    
}
