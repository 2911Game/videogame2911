import java.awt.Container;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;

/**
 * When we click on the button, the application terminates.
 * Probably don't need this in the final one. Doing to learn how to use buttons.
 * 
 * @author emilychen97
 *
 */
public class QuitButton extends JFrame{
	// constructor
	public QuitButton() {
		initUI();
	}
	
	private void initUI() {
		JButton quitButton = new JButton("Quit");
		
		quitButton.addActionListener((ActionEvent event) -> {
			System.exit(0);	// terminates the application
		});
		// place child components in containers, delegate to this function
		createLayout(quitButton);
		
		// other stuff to set up a window
		setTitle("Quit button");
		setSize(300, 200);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	private void createLayout(JComponent... arg) {
		Container pane = getContentPane();
		GroupLayout gl = new GroupLayout(pane);
		pane.setLayout(gl);
		
		// make pretty with padding
		gl.setAutoCreateContainerGaps(true);
		
		gl.setHorizontalGroup(gl.createSequentialGroup().addComponent(arg[0]));
		
		gl.setVerticalGroup(gl.createSequentialGroup().addComponent(arg[0]));
	}
	
	public static void main (String[] args) {
		EventQueue.invokeLater(() -> {
			QuitButton button = new QuitButton();
			button.setVisible(true);
		});
	}
}
