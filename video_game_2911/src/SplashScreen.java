import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * This class draws out the splash screen.
 * 
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public class SplashScreen extends JPanel {
	
	/**
	 * Constructor for SplashScreen.
	 * Sets the dimensions for this panel, preferred size, and
	 * stores the parameter board in its field.
	 * 
	 * @pre board is the same object as in the field of Game that composes this class
	 * @param board	instance of the Board that represents the data of the game
	 */
	public SplashScreen () {
		Dimension dimensions = new Dimension(Dimensions.SPACE * (Dimensions.NUM_COLS + 8), Dimensions.SPACE * Dimensions.NUM_ROWS);
		setPreferredSize(dimensions);
		setFocusable(false);
		setBackground(new Color(139, 219, 104));
		setLayout(new BorderLayout());
		
		URL locSplash = this.getClass().getResource("icons/Pikachu-Push.gif");
        Icon animSplash = new ImageIcon(locSplash);
        JLabel splash = new JLabel(animSplash);
        add(splash, BorderLayout.CENTER);
        
        URL locEnter = this.getClass().getResource("icons/pressEnter.gif");
        Icon animEnter = new ImageIcon(locEnter);
        JLabel enter = new JLabel(animEnter);
        add(enter, BorderLayout.SOUTH);
        
	}
	
	
	/**
	 * Overrides JComponent's paint method to draw all the Item's as well
	 * 
	 * @see JComponent
	 * @param g	Graphics object to be painted
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);
	}
	
	/**
	 * Effectively Removes All the Buttons
	 */
	public void removeAllButtons(){
		this.removeAll();
	}
	
}
