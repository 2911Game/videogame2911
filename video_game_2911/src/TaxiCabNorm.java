/**
 * Implements norm as a taxicab norm. i.e. p_1 metric
 *
 */
public class TaxiCabNorm implements Norm {

	@Override
	public double calcNorm(Coordinate a, Coordinate b) {
		int changeX = a.getX() - b.getX();
		int changeY = a.getY() - b.getY();
		
		return Math.abs(changeX) + Math.abs(changeY);
	}

}
