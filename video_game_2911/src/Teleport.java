import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;

/**
 * Teleportation pads for the game.
 * When you step on the teleport pad you should move the Actor
 * to the destination teleport pad.
 * The game system is in charge of teleporting, the teleport pad will not
 * teleport for you. It just tells you where to teleport to.
 * 
 * 
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 *
 */
public class Teleport extends Actor {

	private Teleport dest;	// destination Teleport pad to teleport to
	
	
	/**
	 * Modified constructor for Teleport to load the iamge.
	 * 
	 * @param x x-coordinate of bottom-right corner of this tile
	 * @param y y-coordinate of bottom-right corner of this tile
	 */
	public Teleport(int x, int y) {
		super(x, y);
		
		URL loc = this.getClass().getResource("icons/teleport.png");
        ImageIcon icon = new ImageIcon(loc);
        Image image = icon.getImage();
        setImage(image);
	}
	
	/**
	 * Setter for where this teleport pad should teleport the player to.
	 * 
	 * @param here	reference to the destination Teleport object
	 */
	public void setDest(Teleport here) {
		dest = here;
	}
	
	/**
	 * Get the reference to which Teleport object this pad teleports to.
	 * 
	 * @return	reference to the destination Teleport object
	 */
	public Teleport getDest() {
		return dest;
	}
	
	
	
}
