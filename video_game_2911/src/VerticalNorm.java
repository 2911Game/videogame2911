
/**
 * The vertical distance between two points.
 *
 */
public class VerticalNorm implements Norm {

	@Override
	public double calcNorm(Coordinate a, Coordinate b) {
		return Math.abs(a.getY() - b.getY());
	}

}
