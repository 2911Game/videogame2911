import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;

/**
 * This class represents a Wall.
 * Constructor specifies an image for this Wall.
 * Wall's cannot be moved.
 * 
 * @author Emily Chen, Justin Ng, Naomi Ng, Patrick Zhou, Rita Yang
 */
public class Wall extends Actor{
	
	/**
	 * Modified constructor from superclass to load an image for this instance.
	 * 
	 * @param x	x-coordinate of bottom-right corner of this tile
	 * @param y	y-coordinate of bottom-right corner of this tile
	 */
	public Wall(int x, int y) {
        super(x, y);

        URL loc = this.getClass().getResource("icons/wall.png");	// look for this resource
        ImageIcon icon = new ImageIcon(loc);	// create an icon from this image
        Image img = icon.getImage();			// create an Image from icon
        setImage(img);							// set the image for this object
    }
	
}
