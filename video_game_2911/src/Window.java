//package video_game_2911;	// not supposed to be in a package?

import java.awt.EventQueue;
import javax.swing.JFrame;

/**
 * Creates the basic window for the video game.
 * Following the Swing tutorial.
 * 
 * @see http://zetcode.com/tutorials/javaswingtutorial/firstprograms/
 * @author Emily Chen (z5098910)
 *
 */
public class Window extends JFrame{
	
	// constructor
	/**
	 * Initializes our basic window.
	 * @post window should pop up
	 */
	public Window() {
		// private initialization functions
		initUI();
	}
	
	/**
	 * Initialize the window. Helper method for Window constructor.
	 */
	private void initUI() {
		setTitle("A Fun Game");			// title at the top of the window
		setSize(500, 500);				// window is 300 px wide, 200 px tall
		setLocationRelativeTo(null);	// centers the window
		setDefaultCloseOperation(EXIT_ON_CLOSE);	// closes if we click the Close button
		
	}
	
	// main, move this into Game class later ================== TO-DO =====================
	public static void main(String[] args) {
		
		// what is this arrow notation?
		EventQueue.invokeLater(() -> {
			Window game = new Window();
			game.setVisible(true);
		});
		
	}
	
	
}
